# 1. imports and global variables
import numpy as np
from netCDF4 import *
import sys
import matplotlib.lines as mtplines
import matplotlib.pyplot as plt
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "cerrado"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] = 5 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
burningdates = [1,3]
burningdates_label = ['Early Season','End Season']
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']

# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 2013 # 99 = no el nino 93 2013
plot_config['end_year'] = plot_config['start_year']+1
plot_config['nb_years'] = plot_config['end_year'] - plot_config['start_year']
plot_config['start_day'] = (plot_config['start_year'] - run_config['start_year'])*365
plot_config['end_day'] = (plot_config['end_year'] - run_config['start_year'])*365-5
plot_config['nb_days'] = plot_config['end_day'] - plot_config['start_day']
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
plot_config['plot_title'] = (f"Daily Leaf Carbon at Cerrado site within {run_config['practice_term']} term practice "
                             f"{run_config['livestock_density'][0]}.{run_config['livestock_density'][1]} "
                             f"LSU/ha and {run_config['burning_frequency']}-year burning frequency scenario for year {plot_config['start_year']}")
plot_config['pdf_path'] = "plot_overview"
plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11
linestyle_collection = ['solid','dashed']
plt.figure(figsize=(20, 3))

legend = []
for i,burningdate in enumerate(burningdates) :

    if run_config['burning_frequency'] == 0 :
        run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
        run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
    else :
        run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
        run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
        run_config['burning_frequency'],run_config['burning_phase'],burningdate)

    nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
    leafc = nc.variables['LeafC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
    nc.close()

    nc = Dataset("{}/burningdate.nc".format(run_config['folder_path']), mode='r')
    burnday = nc.variables['BURNINGDATE'][plot_config['start_year']-run_config['start_year']:plot_config['start_year']-run_config['start_year']+1,0,0] #burning date/yr
    nc.close()

    line, = plt.plot(leafc,color="#000000",label=f"'{burningdates_label[i]}'",linestyle=linestyle_collection[i])
    legend.append(line)

    pt = plt.scatter(burnday[0]-1,leafc[int(burnday[0])-1], color="#800000", s=70, label='Burning event')

legend.append(pt)
plt.legend(handles=legend,fontsize=13)
plt.title("Daily leaf carbon at Cerrado Site in 2013 - long term scenario, 0.1 LSU/ha, 5-year burning frequency",fontsize=20)
monthly_ticks = np.cumsum([0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30])
plt.xticks(monthly_ticks,['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
plt.tick_params(labelsize=13, length=4, direction="inout")
plt.xlabel('Time',fontsize=15)
plt.ylabel(f'LeafC (gC/m2)',fontsize=15)
plt.grid(True)
plt.xlim(0, 360)
plt.savefig(f"figures_output/daily_leafc_bd_{plot_config['pdf_file']}.png", bbox_inches='tight')
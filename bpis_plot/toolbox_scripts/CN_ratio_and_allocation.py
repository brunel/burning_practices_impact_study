"""
This Python script analyzes and visualizes data related to the impact of burning practices on leaf carbon (C) and nitrogen (N) dynamics in a biome (in this case, "cerrado"). The script is divided into several key sections:

1. **Imports and Global Variables**:
   - The script imports necessary libraries, including `numpy`, `bpis_plot_support_function`, `netCDF4`, and `matplotlib.pyplot`.
   - The global configuration settings for the simulation are set, defining parameters like the biome, pixel, and file paths for simulation results. These configurations also specify details for the study, such as practice term (long-term), livestock density (01), burning frequency (5 years), and burning phase and date.

2. **Run Configuration**:
   - The script sets up paths for accessing simulation data, based on the configuration settings, such as folder paths for different burning scenarios (e.g., no burning, burning at specified frequency).
   - It also calculates the number of years of interest (from 1948 to 2017).

3. **Figure Configuration**:
   - A figure is prepared for visualizing the data, with subplots defined for specific time periods (2003-2004 in this case). The start and end dates for plotting are also calculated, and relevant plot configurations, including titles and font sizes, are defined.

4. **Data Loading**:
   - The script loads daily data on leaf carbon (`d_cleaf.nc`) and nitrogen (`d_nleaf.nc`) for the specified time range using the `Dataset` class from the `netCDF4` module. The data is then extracted for the first pixel in the biome.

5. **Plotting Leaf C and N**:
   - The script creates a plot for the daily leaf carbon and nitrogen dynamics. Two y-axes are used: one for leaf carbon and one for leaf nitrogen. The script plots the leaf carbon and nitrogen data, with a red vertical dashed line marking the "burning event."
   - The plot is configured with proper axis labels, titles, and legends.

6. **Plotting C:N Ratio**:
   - The script calculates the leaf C:N ratio by dividing leaf carbon by leaf nitrogen. It creates a similar plot for the C:N ratio, with another red vertical dashed line indicating the burning event.

7. **Saving Plots**:
   - Both the leaf carbon and nitrogen plot, as well as the C:N ratio plot, are saved as PNG files (`daily_leaf_c_n_cerrado.png` and `cnratio_allocation_cerrado.png`), with appropriate figure layout settings for clarity.

Overall, the script is designed to examine how different burning scenarios affect the carbon and nitrogen dynamics in the cerrado biome, with a focus on the temporal dynamics of leaf carbon, nitrogen, and their ratio around the burning event. It provides a visual overview of the data with well-labeled plots for interpretation.
"""
# 1. imports and global variables
import numpy as np
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "cerrado"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] = 5 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 1 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']

# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 2003 # 99 = no el nino
plot_config['end_year'] = 2004
plot_config['nb_years'] = plot_config['end_year'] - plot_config['start_year']
plot_config['start_day'] = (plot_config['start_year'] - run_config['start_year'])*365
plot_config['end_day'] = (plot_config['end_year'] - run_config['start_year'])*365-5
plot_config['nb_days'] = plot_config['end_day'] - plot_config['start_day']
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "plot_overview"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11


# daily leaf C and N
# daily leave C
nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
leafc = nc.variables['LeafC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

# daily leave N
nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
leafn = nc.variables['nleaf'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

# Leaf C
fig, ax1 = plt.subplots(figsize=(20, 3))
line, = ax1.plot(leafc,color="#000000",label='leaf C')
legend_collection = []
legend_collection.append(line)

# Leaf N
ax2 = ax1.twinx()
line, = ax2.plot(leafn,color="#000000",linestyle='dashed',label='leaf N')
legend_collection.append(line)

# BURNING DATE
nc = Dataset("{}/burningdate.nc".format(run_config['folder_path']), mode='r')
id_year = plot_config['start_year'] - run_config['start_year']
burningday = nc.variables['BURNINGDATE'][id_year:id_year+1,0,0] #burning date/yr
nc.close()

line = ax1.axvline(x=burningday-1, color='r', linestyle='--', linewidth=2, label='burning event')
legend_collection.append(line)

plt.title(f"Leaf C:N ratio and allocation at {run_config['biome']} site in {plot_config['start_year']} - "
          f"long term scenario, 0.1 LSU/ha, 'early-burning' and 5-year burning",fontsize=20)

monthly_ticks = np.cumsum([0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30])
plt.xticks(monthly_ticks,['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

ax1.set_ylabel('Leaf C (gC/m2)',fontsize=15)
ax2.set_ylabel('Leaf N (gN/m2)',fontsize=15)
ax1.tick_params(labelsize=13, length=4, direction="inout")
ax2.tick_params(labelsize=13, length=4, direction="inout")

plt.xlim(0, 360)

ax1.legend(handles = legend_collection,fontsize=13,loc = 'best',ncol=2)
plt.grid(True)
plt.savefig(f"figures_output/daily_leaf_c_n_cerrado.png", bbox_inches='tight')

# C:N ratio
cn_ratio = np.divide(leafc,leafn)
fig, ax1 = plt.subplots(figsize=(20, 3))
line, = ax1.plot(cn_ratio,color="#000000",label='leaf C')
legend_collection = []
legend_collection.append(line)

line = ax1.axvline(x=burningday-1, color='r', linestyle='--', linewidth=2, label='burning event')
legend_collection.append(line)

monthly_ticks = np.cumsum([0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30])
plt.xticks(monthly_ticks,['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
ax1.set_ylabel('Leaf C:N ratio',fontsize=15)

ax1.tick_params(labelsize=13, length=4, direction="inout")
plt.xlim(0, 360)
ax1.legend(handles = legend_collection,fontsize=13,loc = 'best',ncol=2)
plt.grid(True)
plt.savefig(f"figures_output/cnratio_allocation_cerrado.png", bbox_inches='tight')
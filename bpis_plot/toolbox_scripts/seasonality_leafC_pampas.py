#libraries to import
from pylab import *
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt
import pandas as pd

# 2. configuration
# a) run configuration
climate = "temperature"

run_config = dict()
run_config['biome'] = "pampas"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] = 0 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 4 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']

# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 1948 # 99 = no el nino
plot_config['end_year'] = 2017
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "plot_overview"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11

# BURNING DATE
nc = Dataset("{}/burningdate.nc".format(run_config['folder_path']), mode='r')
burningdates = nc.variables['BURNINGDATE'][:,0,0] #burning date/yr
nc.close()

burning_period = [[6,7.75],[6.75,8.5],[7.25,9.25],[7.75,10]]

#leaf c
nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
day_list = nc.variables['time'][:]
first_day = day_list[(plot_config['start_year'] - run_config['start_year']) * 365]
last_day = day_list[(plot_config['end_year'] - run_config['start_year']) * 365]
id_first_day = np.where(day_list == first_day)[0][0]
id_last_day = np.where(day_list == last_day)[0][0]
npp = nc.variables['LeafC'][id_first_day:id_last_day,0,0] #gC/m2/days
nc.close()

npp = sf.daily_to_monthly(npp, "mean") #gC/m2/months
dates = pd.date_range(start=f'{plot_config["start_year"]}-01-01', periods=len(npp), freq='M')
# Create a DataFrame
df = pd.DataFrame(npp, index=dates, columns=['value'])
# Ensure the index is a datetime index
df.index = pd.to_datetime(df.index)
# Group by month and calculate the mean
monthly_average = df.resample('M').mean().groupby(df.index.month).mean()
npp = monthly_average['value'].tolist()

# NPP
fig, ax1 = plt.subplots(figsize=(20, 3))
line, = ax1.plot(npp,color="#000000",label='leaf C')
ax1.set_ylim(250,320)
legend_collection = []
legend_collection.append(line)

if climate == "temperature":

    # TEMPERATURE
    nc = Dataset(f"{run_config['run_collection_path']}/temperature.nc", mode='r') # f""
    day_list = nc.variables['time'][:]
    first_day = day_list[(plot_config['start_year'] - run_config['start_year']) * 365]
    last_day = day_list[(plot_config['end_year'] - run_config['start_year']) * 365]
    id_first_day = np.where(day_list == first_day)[0][0]
    id_last_day = np.where(day_list == last_day)[0][0]
    temperature = nc.variables['temp'][id_first_day:id_last_day,0,0] #gC/m2/days
    nc.close()

    temperature = sf.daily_to_monthly(temperature, "mean") #gC/m2/months
    dates = pd.date_range(start=f'{plot_config["start_year"]}-01-01', periods=len(temperature), freq='M')
    # Create a DataFrame
    df = pd.DataFrame(temperature, index=dates, columns=['value'])
    # Ensure the index is a datetime index
    df.index = pd.to_datetime(df.index)
    # Group by month and calculate the mean
    monthly_average = df.resample('M').mean().groupby(df.index.month).mean()
    temperature = monthly_average['value'].tolist()


    # Temperature
    ax2 = ax1.twinx()
    line, = ax2.plot(temperature,color="#800000",label='Temperature')
    legend_collection.append(line)

elif climate == "precipitation":
    # PRECIPITATION
    nc = Dataset("{}/mprec.nc".format(run_config['folder_path']), mode='r')
    months = nc.variables['time'][:]
    prec = nc.variables['prec'][:,0,0] #gC/m2/days
    nc.close()

    prec = prec.reshape(70, 12)
    monthly_avg_prec = np.mean(prec, axis=0)

    ax2 = ax1.twinx()
    line, = ax2.plot(monthly_avg_prec,color="#800000",label='Precipitation')
    legend_collection.append(line)

if run_config['biome'] == "mata2":
    run_config['biome'] = "North Atlantic Forest"
plt.title(f"Averaged monthly LeafC and temperature at the {run_config['biome']} site",fontsize=20)

months_ticks = range(0,12, 1)
plt.xticks(months_ticks,['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

ax1.set_ylabel('Leaf C (gC/m2)',fontsize=15)
ax2.set_ylabel('Temperature (°C)',fontsize=15)
ax1.tick_params(labelsize=13, length=4, direction="inout")
ax2.tick_params(labelsize=13, length=4, direction="inout")

plt.xlim(0, 11)

# ADD BURNING PERIOD
linestyles = [ "solid", "dotted", "dashed" , "dashdot"]
strategy=["Early Season","Late Season","End Season","Early Spring"]
for i,burningp in enumerate(burning_period):
    line = ax1.axhline(y=175+i*2,xmin=(burningp[0]-1)/10,xmax=(burningp[1]-1)/10,color="#808080",linestyle=linestyles[i], linewidth=2, label=strategy[i])
    legend_collection.append(line)

ax1.legend(handles = legend_collection,fontsize=13,loc = 'best',ncol=2)

plt.grid(True)
plt.savefig(f"figures_output/monthly_leafc_{climate}_pampas_{run_config['livestock_density']}.png", bbox_inches='tight')
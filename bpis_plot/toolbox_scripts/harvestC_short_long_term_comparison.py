"""
This script is designed to generate a plot comparing carbon harvest data for short-term and long-term practices in a "cerrado" biome under various grazing and burning conditions. Here's a description of its main components:

### Libraries and Modules:
1. **`pylab`**: Imports various scientific functions from `matplotlib` and `numpy`.
2. **`bpis_plot_support_function as sf`**: A custom module for supporting plotting and data manipulation functions.
3. **`netCDF4`**: Used to read NetCDF files, which store data for various environmental parameters.
4. **`matplotlib.pyplot as plt`**: Used for plotting graphs.

### Configuration:
The script sets up a number of configurations:
- **Run Configuration** (`run_config`): Specifies details for the biome (`cerrado`), pixel, livestock density, burning frequency, and years (1948 to 2017). It also determines file paths for the data and burning conditions, defining whether the run is short or long-term.
- **Plot Configuration** (`plot_config`): Configures aspects of the figure, such as its size, the title, axis labels, and the path for saving the plot. It also sets the time range for analysis (in this case, from 1948 to 2017).

### Data Processing:
1. **Short-Term Carbon Harvest**:
   - The script reads carbon harvest data for a short-term scenario from a NetCDF file (`d_mgrass_harvestc.nc`), adjusts the data (since harvest output counts only 75% of total harvest), and then converts it from daily to annual data.

2. **Long-Term Carbon Harvest**:
   - Similarly, it reads the long-term scenario data from a different file and processes it in the same way, adjusting for the same harvest percentage and converting the data to annual totals.

### Plotting:
1. The **carbon harvest data** for both short-term and long-term practices are plotted on the same graph, with short-term data shown in solid lines and long-term data in dashed lines.
2. A **red dashed line** indicates the nutritional requirement threshold for livestock, which is calculated based on livestock density.
3. The plot is titled appropriately to describe the biome, practice type (short/long), livestock density, and burning regime.
4. The **x-axis** represents years, and the **y-axis** represents the harvested carbon in grams per square meter per year (gC/m²/yr).
5. The plot also includes a **legend** and grid for better readability.

### Output:
The resulting plot, comparing carbon harvests for short-term and long-term practices along with the nutritional requirement line, is saved as a PNG file (`timeserie_annual_harvestC_cerrado.png`) in the `figures_output` directory.

This script essentially provides a visualization of how carbon harvest in the cerrado biome differs under short-term versus long-term practices, while also accounting for the livestock density and burning frequency.
"""
#libraries to import
from pylab import *
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "cerrado"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "short" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] = 5 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 1 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']

# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 1948 # 99 = no el nino
plot_config['end_year'] = 2017
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "plot_overview"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11


# open harvestC
# short term
nc = Dataset(f"{run_config['folder_path']}/d_mgrass_harvestc.nc", mode='r')
harvestc_short = nc.variables['d_mgrass_harvestc'][:,0,0] #gC/m2/days
nc.close()
harvestc_short = harvestc_short/3*4 #harvest output only count 75% of total harvest
harvestc_short = sf.daily_to_annual(harvestc_short, "sum") #gC/m2/yrs

long_term_folder_path = "{}{}_longterm_ld{}_fq{}-{}_bd{}".format(run_config['run_collection_path'],run_config['pixel'],
                                                               run_config['livestock_density'],
                                                               run_config['burning_frequency'],run_config['burning_phase'],
                                                               run_config['burning_date'])

# long term
nc = Dataset(f"{long_term_folder_path}/d_mgrass_harvestc.nc", mode='r')
harvestc_long = nc.variables['d_mgrass_harvestc'][:,0,0] #gC/m2/days
nc.close()
harvestc_long = harvestc_long/3*4 #harvest output only count 75% of total harvest
harvestc_long = sf.daily_to_annual(harvestc_long, "sum") #gC/m2/yrs

limit = 4000*365*1e-4*int(run_config['livestock_density'])/10  #gC/m2/yr
limit = limit*80/100 #80% requirement

fig, ax1 = plt.subplots(figsize=(20, 3))

line, = ax1.plot(harvestc_short,color="#000000",label='short term practice')
legend_collection = []
legend_collection.append(line)

line, = ax1.plot(harvestc_long,color="#000000",linestyle='dashed',label='long term practice')
legend_collection.append(line)

line = ax1.axhline(y=limit,xmin=0,xmax=70, color='r', linestyle='--', linewidth=2, label='80% nutritional requirement')
legend_collection.append(line)

plt.title(f"Harvest Carbon at {run_config['biome']} site - "
          f"short/long term scenario, 0.1 LSU/ha, 'early-burning' and 5-year burning",fontsize=20)

year_ticks = range(2, run_config['nb_year'], 5)
plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])

ax1.set_ylabel('Harvest C (gC/m2)',fontsize=15)
ax1.tick_params(labelsize=13, length=4, direction="inout")

plt.xlim(0, 69)

ax1.legend(handles = legend_collection,fontsize=13,loc = 'best',ncol=2)
plt.grid(True)
plt.savefig(f"figures_output/timeserie_annual_harvestC_cerrado.png", bbox_inches='tight')
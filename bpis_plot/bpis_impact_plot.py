"""
### Script Description

This Python script analyzes the impact of burning practices on carbon emissions and ecological parameters (such as harvest, leaf carbon-to-nitrogen (C:N) ratio, and soil C:N ratio) in different biomes. It calculates and visualizes the effect of burning strategies, frequency, and term duration on these parameters, incorporating livestock presence as a factor. The results are presented in scatter plots with corresponding legends for easier interpretation.

### Key Features:

1. **Configuration**:
   - Biomes: The script runs analyses for multiple biomes including Cerrado, Caatinga, South Atlantic Forest, and North Atlantic Forest.
   - Burning Strategies: It considers different burning strategies (early-season, late-season, end-season, and early-spring).
   - Frequency & Term Duration: The script evaluates burning frequencies (2-year, 5-year, and 10-year) and practice durations (short and long-term).

2. **Data Processing**:
   - It imports data for different scenarios (burning dates, frequencies, and terms) and computes the impact on ecological parameters.
   - Harvest, leaf C:N ratio, and soil C:N ratio are computed for each scenario and normalized for comparison.
   - CO2 emissions (in terms of GWP - Global Warming Potential) are calculated for each scenario using the `annual_gwp_calculation` function.

3. **Normalization & Impact Calculation**:
   - The data for harvest, leaf C:N ratio, and soil C:N ratio are normalized based on their maximum values for comparison across different scenarios.
   - The overall impact is computed as an average of the normalized values, weighted according to the significance of each parameter.

4. **Visualization**:
   - The script generates scatter plots displaying the relationship between the normalized impact and CO2 emissions for each biome.
   - Different markers represent different burning frequencies, and colors represent various burning strategies.
   - The plots are tailored with axis labels, titles, and grid lines for easy interpretation.

5. **Legend Creation**:
   - A separate legend is generated to explain the markers used for different burning frequencies, strategies, and practice durations.
   - The legend is saved as a separate image file for clarity in presentations or reports.

6. **Output**:
   - The resulting scatter plot and legend are saved as PNG files (`impact_plot.png` and `impact_plot_legend.png`) for further use.

This script provides a comprehensive tool for assessing and visualizing the impact of burning practices on ecological processes and carbon emissions across various biomes.
"""
from bpis_plot_support_function import output_dictionnary,gwp_calculation,import_monthly_data,import_daily_data,daily_to_annual,monthly_to_annual
from bpis_plot_support_function import exclusion_scenario, get_biome_pixel, annual_harvestc, annual_leaf_cnratio, annual_soil_cnratio, set_subplots,annual_gwp_calculation
from netCDF4 import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mtplines
import matplotlib.patches as mpatches


biome_list = ["cerrado","caatinga","mata1","mata2"]
biome_name = ["Cerrado","Caatinga","South Atlantic Forest","North Atlantic Forest"]
density_list = ["01"]
density = density_list[0]

term_list = ["short","long"]
marker_size_list = [80,200]

frequency_list = [[2,0], [2,1],
                  [5,0], [5,1], [5,2], [5,3], [5,4],
                  [10,0], [10,1], [10,2], [10,3], [10,4], [10,5], [10,6], [10,7], [10,8], [10,9]]
main_frequency_list = [2,5,10]
id_main_frequency_list = [0,1,2]

color_list = ['#fdd0a2','#fd8d3c','#d94801','#7f2704']
marker_list = ["o","X","^"]

burning_date_list = [1,2,3,4]
burning_strategy = ["early-season","late-season","end-season","early_spring"]



run_path = "C:/Users/brune\Documents\local_data_cluster"

fig = plt.figure(figsize=(25, 25))
subplots = set_subplots(2, 2, 0.2, 0.2, 0.03, 0.03)

for id_biome, biome in enumerate(biome_list):

    axe0 = fig.add_axes(subplots[id_biome])

    pixel = get_biome_pixel (biome)

    # FIRST ROUND TO GET ABSOLUTE MAXIMUM
    annual_harvest = {"data":[]}
    annual_leaf_cn_ratio = {"data":[]}
    annual_soil_cn_ratio = {"data":[]}

    for id_term,term in enumerate (term_list) :

        folder_no_fire = f"{run_path}/{biome}_{pixel}/{pixel}_{term}term_ld{density}_nofire"
        harvest_ref = annual_harvestc(folder_no_fire)
        leaf_cnratio_ref = annual_leaf_cnratio(folder_no_fire)
        soil_cnratio_ref = annual_soil_cnratio(folder_no_fire)

        for id_date, date in enumerate (burning_date_list) :
            for id_frequency,frequency in enumerate (frequency_list) :
                freq = frequency[0]

                if exclusion_scenario(biome,freq,date,term):
                    continue

                folder_run = f"{run_path}/{biome}_{pixel}/{pixel}_{term}term_ld{density}_fq{frequency[0]}-{frequency[1]}_bd{date}"

                harvest = annual_harvestc(folder_run) - harvest_ref
                leaf_cnratio = leaf_cnratio_ref - annual_leaf_cnratio(folder_run)
                soil_cnratio = soil_cnratio_ref - annual_soil_cnratio(folder_run)

                annual_harvest["data"] = np.concatenate((annual_harvest["data"],harvest))
                annual_leaf_cn_ratio["data"] = np.concatenate((annual_leaf_cn_ratio["data"],leaf_cnratio))
                annual_soil_cn_ratio["data"] = np.concatenate((annual_soil_cn_ratio["data"],soil_cnratio))

    # normalisation
    # harvest
    annual_harvest["max"] = np.amax(np.abs(annual_harvest["data"]))
    # leaf cn ratio
    annual_leaf_cn_ratio["max"] = np.amax(np.abs(annual_leaf_cn_ratio["data"]))
    # soil cn ratio
    annual_soil_cn_ratio["max"] = np.amax(np.abs(annual_soil_cn_ratio["data"]))


    for id_date, date in enumerate (burning_date_list) :

        for id_term,term in enumerate (term_list) :

            folder_no_fire = f"{run_path}/{biome}_{pixel}/{pixel}_{term}term_ld{density}_nofire"

            harvest_ref = annual_harvestc(folder_no_fire)
            leaf_cnratio_ref = annual_leaf_cnratio(folder_no_fire)
            soil_cnratio_ref = annual_soil_cnratio(folder_no_fire)

            for id_frequency,main_frequency in enumerate(main_frequency_list):
                norma_list = []
                annual_co2_emission_list = []

                for frequency in frequency_list :
                    freq = frequency[0]
                    if freq != main_frequency :
                        continue

                    if exclusion_scenario(biome,main_frequency,date,term):
                        continue

                    folder_run = f"{run_path}/{biome}_{pixel}/{pixel}_{term}term_ld{density}_fq{frequency[0]}-{frequency[1]}_bd{date}"

                    # min max normalisation
                    harvest = annual_harvestc(folder_run) - harvest_ref
                    leaf_cnratio = leaf_cnratio_ref - annual_leaf_cnratio(folder_run)
                    soil_cnratio = soil_cnratio_ref - annual_soil_cnratio(folder_run)

                    # normalisation
                    harvest_norma = harvest / annual_harvest["max"]
                    leaf_cnratio_norma = leaf_cnratio / annual_leaf_cn_ratio["max"]
                    soil_cnratio_norma = soil_cnratio / annual_soil_cn_ratio["max"]

                    norma = np.average(harvest_norma*0.5 + leaf_cnratio_norma*0.25 + soil_cnratio_norma*0.25)
                    norma_list.append(norma)

                    annual_co2_emission = np.sum(annual_gwp_calculation(folder_run))/70
                    annual_co2_emission_list.append(annual_co2_emission)

                axe0.scatter(np.average(norma_list), np.average(annual_co2_emission_list),
                             c=color_list[id_date], # color = burning date
                             marker=marker_list[id_frequency], # marker = frequency
                             s = marker_size_list[id_term]) # size = term

        if id_biome == 0 or id_biome == 2:
            axe0.set_ylabel(f"Carbon emission (co2e/year)", size=15)

        if id_biome < 2:
            axe0.set(xlim= (-0.5, 0.5),ylim= (0, 0.75))
        else :
            axe0.set(xlim= (-0.5, 0.5),ylim= (0, 0.75))

        if id_biome >= 2:
            axe0.set_xlabel("Impact", size=15)

        axe0.axvline(x=0, color='grey', linestyle='--')

        axe0.tick_params(labelsize=13, length=4, direction="inout")

        axe0.set_title(f"{biome_name[id_biome]} location",fontsize=15)
        axe0.grid()

fig.savefig(f"figures_output/impact_plot.png", bbox_inches='tight', orientation='landscape')


# LEGEND
marker_small = 5
marker_big = 10
legend = []
# frequency
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", label="Burning Frequency"))
for marker in marker_list :
    legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", marker=marker, markersize=marker_small, label="2-year"))
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", label=""))
# burning strategy
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", label="Burning Strategy"))
for i,color in enumerate(color_list):
    legend.append(mtplines.Line2D([0], [0], color=color, linestyle="none", marker="o", markersize=marker_small, label=burning_strategy[i]))
# term practice
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", label="Practice Duration"))
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", marker="o", markersize=marker_small, label="short term"))
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", marker="o", markersize=marker_big, label="long term"))
legend.append(mtplines.Line2D([0], [0], color="#000000", linestyle="none", label=""))
fig = plt.figure(figsize=(25, 25))
subplots = set_subplots(1, 1, 0.2, 0.2, 0.01, 0.06)

axe = fig.add_axes(subplots[0],frame_on=False, yticks=[],xticks=[])
axe.legend(handles = legend,fontsize=14,loc = 'lower left',ncol=3)
axe.grid()

fig.savefig(f"figures_output/impact_plot_legend.png", bbox_inches='tight', orientation='landscape')






#libraries to import
from netCDF4 import *
from pylab import *
from matplotlib.backends.backend_pdf import PdfPages
rc('text', usetex=False)
from tqdm import tqdm
from bpis_plot_support_function import get_biome_pixel,daily_to_annual,monthly_to_annual
import os
from excluded_scenario import import_excluded_scenario
excluded_scenario = import_excluded_scenario()

#text fontsize
scale = -10
fs_textongraph = 21 + scale
fs_label = 22 + scale
fs_axistitle = 25 + scale
fs_textoutgraph = 25+ scale
fs_title = 24+ scale
fs_legend = 25+ scale

def write_list_to_txt(list_of_strings, file_name):
    with open(file_name + '.txt', 'w') as file:
        for string in list_of_strings:
            file.write(string + '\n')

def test_maximum (tmp_maximum,mosaic):
    if tmp_maximum != None:
        if np.amax(mosaic) > tmp_maximum :
            tmp_maximum = np.amax(mosaic)
    else :
        tmp_maximum = np.amax(mosaic)
    return tmp_maximum

def test_minimum (tmp_minimum,mosaic):

    if tmp_minimum != None:
        if np.amin(mosaic) < tmp_minimum :
            tmp_minimum = np.amin(mosaic)
    else :
        tmp_minimum = np.amin(mosaic)
    return tmp_minimum

def set_subplots (n,m,h,l,eh,ev):
    ''' return a list of subplotpython how to [xposition,yposision,width,high]\n
    n: number of horizontal plots; m: number of vertical plots;\n
    h: high of plots; l: larger of plots;\n
    eh: space between plots horizontally; ev: space between plots vertically\n
    '''
    subplots = []
    for j in range (m-1,-1,-1):
        for i in range (0,n,1):
            subplots.append([i*(l+eh),j*(h+ev),l,h])
    return (subplots)


def monthly_to_yearly(monthly_data,nb_years):

    yearly_data = np.zeros((nb_years))
    id_year = 0
    for j in range (0,len(monthly_data),12):
        yearly_data[id_year] = np.nansum(monthly_data[j:j+12])
        id_year += 1
    return(yearly_data)

#global_declaration()

def harvest_c ():

    print("harvest c plotting ...")
    title = "DMI {}-{} (gC/m2)".format(year1,year2)

    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### HARVESTC
                    ### harvestc(time,npft,latitude,longitude)
                    nc = Dataset("{}/d_mgrass_harvestc.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = np.where(days == (year1-1948)*365)[0][0]
                    d2 = np.where(days == (year2-1948)*365)[0][0]+1
                    harvestc = nc.variables['d_mgrass_harvestc'][d1:d2,0,0] #gC/m2/yr
                    harvestc = harvestc/3*4 #harvest output only count 75% of total harvest
                    harvestc = daily_to_annual(harvestc, "sum")
                    nc.close()
                    mosaic[l,k] = np.average(harvestc[:])


            #harvest requirement depends on livestock density
            limit = 4000*365*1e-4*rdensity  #gC/m2/yr
            limit80 = limit*80/100 #80% requirement
            #convertion to DMI
            limit = limit / 0.41
            limit80 = limit80 /0.41

            # convertion to DMI
            mosaic = mosaic/0.41

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(smosaic*(-1),cmap="YlOrRd",vmin=-limit,vmax=-limit80,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    if imp_excluded_scenario[l,k] == 0:
                        axe.text(k,l,"{} ±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),
                                 fontsize = fs_textongraph,ha="center",va="center",weight="bold")
                    else :
                        axe.text(k,l,"{} ±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),
                                 fontsize = fs_textongraph,ha="center",va="center",weight="bold",color="#ffffff")

    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))
    mosaiclimit = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)


            ### HARVESTC
            ### harvestc(time,npft,latitude,longitude)
            nc = Dataset("{}/d_mgrass_harvestc.nc".format(folder), mode='r')
            days = nc.variables['time'][:]
            d1 = np.where(days == (year1-1948)*365)[0][0]
            d2 = np.where(days == (year2-1948)*365)[0][0]+1
            harvestc = nc.variables['d_mgrass_harvestc'][d1:d2,0,0] #gC/m2/yr
            harvestc = harvestc/3*4 #harvest output only count 75% of total harvest
            harvestc = daily_to_annual(harvestc, "sum")
            nc.close()

            mosaic[i,j] = np.average(harvestc[:])
            mosaic = mosaic /0.41

            #harvest requirement depends on livestock density
            limit = 4000*365*1e-4*rdensity  #gC/m2/yr
            limit80 = limit*80/100 #80% requirement
            #convertion to DMI
            limit = limit/0.41
            limit80 = limit80/0.41

            #mask where harvest < requirement threshold (80% of requirement)
            if mosaic[i,j] > limit80 :
                mosaiclimit[i,j] = mosaic[i,j]

    mosaiclimit = ma.masked_where(mosaiclimit <= 0.001, mosaiclimit)

    # convertion to DMI
    mosaiclimit = mosaiclimit / 0.41

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(mosaiclimit*(-1),cmap="YlOrRd")#,vmin=-limit,vmax=-limit80) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_harvestc.png", bbox_inches='tight',orientation = 'landscape')

def leave_c (maximum):

    print("leaf c plotting ...")
    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "LeafC (gC/m2)"
    tmp_maximum = None

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic
            reco_mosaic = np.zeros((len(dates),len(frequencys)))
            recovery_mosaic = np.zeros((len(dates),3))

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### LEAVE C
                    ### C_leaf_pft(time,npft,latitude,longitude)

                    nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = np.where(days == (year1-1948)*365)[0][0]
                    d2 = np.where(days == (year2-1948)*365)[0][0]+1
                    leavec = nc.variables['LeafC'][d1:d2,0,0] #gC/m2/day
                    nc.close()

                    #average over time period (last 20 years)
                    mosaic[l,k] = np.average(leavec[:])

                    nc = Dataset("{}/d_npp.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = np.where(days == (year1-1948)*365)[0][0]
                    d2 = np.where(days == (year2-1948)*365)[0][0]+1
                    npp = nc.variables['NPP'][d1:d2,0,0] #gC/m2/day
                    nc.close()

                    ### BURNINGDATE
                    ### BURNINGDATE(time,latitude,longitude)
                    nc = Dataset("{}/burningdate.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    burningdates = nc.variables['BURNINGDATE'][y1:y2,0,0]
                    nc.close()

                    # recovery time
                    recovery_t = []
                    for id_year,year in enumerate(years[y1:y2]) :

                        if burningdates[id_year] > 0:
                            burn_index = int(id_year*365 + burningdates[id_year])
                            recovery_t.append(np.sum(npp[burn_index:burn_index+30]))

                    reco_mosaic[l,k] = np.average(recovery_t)
                    if reco_mosaic[l,k] < 0 :
                        reco_mosaic[l,k] = 0

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            recovery_mosaic[:,0] = np.average(reco_mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            recovery_mosaic[:,1] = np.average(reco_mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)
            recovery_mosaic[:,2] = np.average(reco_mosaic[:,7:],axis=1)

            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=0,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     f"{title} - {term} term practice\nlivestock density {rdensity} LSU/ha\n",
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{} ±{}\n[{}]".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2),round(recovery_mosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")


    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)


            ### LEAVE C
            ### C_leaf_pft(time,npft,latitude,longitude)

            nc = Dataset("{}/pft_cleaf.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            npfts = chartostring(nc.variables['NamePFT'][:])
            idpasture = np.where(npfts=="pasture")[0][0]
            leavec = nc.variables['C_leaf_pft'][y1:y2,idpasture,0,0] #gC/m2/yr
            nc.close()

            #last 20 years of time series 1997-2017
            mosaic[i,j] = np.average(leavec[:])


            tmp_maximum = test_maximum (tmp_maximum,mosaic)

    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=0,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tleave_c ({ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_leavec.png", bbox_inches='tight',orientation = 'landscape')

def leave_cn_ratio (minimum,maximum,reference):

    print("leaf cn ratio plotting ...")

    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "Leaf CN ratio {}-{}".format(year1,year2)
    tmp_minimum = None
    tmp_maximum = None

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### Leave C
                    ### LeafC(time,latitude,longitude)
                    nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = np.where(days == (year1-1948)*365)[0][0]
                    d2 = np.where(days == (year2-1948)*365)[0][0]+1
                    dleavec = nc.variables['LeafC'][d1:d2,0,0] #gC/m2
                    nc.close()

                    leavec = np.zeros((year2-year1+1))
                    for y,year in enumerate(np.arange(year1,year2+1,1)):
                        leavec[y] = np.average(dleavec[365*y:365*y+365])

                    ### LEAVE N
                    ### nleaf(time,latitude,longitude)
                    nc = Dataset("{}/d_nleaf.nc".format(folder), mode='r')
                    dleaven = nc.variables['nleaf'][d1:d2,0,0] #gN/m2
                    nc.close()

                    leaven = np.zeros((year2-year1+1))
                    for y,year in enumerate(np.arange(year1,year2+1,1)):
                        leaven[y] = np.average(dleaven[365*y:365*y+365])

                    #cn ratio
                    cnratio = np.divide(leavec,leaven)
                    #last 20 years of time series 1997-2017
                    cnratio9717 = np.average(cnratio)
                    mosaic[l,k] = cnratio9717 / reference

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=minimum,vmax=maximum,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{} ±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")



    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)

            ### Leave C
            ### LeafC(time,latitude,longitude)
            nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
            days = nc.variables['time'][:]
            d1 = np.where(days == (year1-1948)*365)[0][0]
            d2 = np.where(days == (year2-1948)*365)[0][0]+1
            dleavec = nc.variables['LeafC'][d1:d2,0,0] #gC/m2
            nc.close()

            leavec = np.zeros((year2-year1+1))
            for y,year in enumerate(np.arange(year1,year2+1,1)):
                leavec[y] = np.average(dleavec[365*y:365*y+365])

            ### LEAVE N
            ### nleaf(time,latitude,longitude)
            nc = Dataset("{}/d_nleaf.nc".format(folder), mode='r')
            dleaven = nc.variables['nleaf'][d1:d2,0,0] #gN/m2
            nc.close()

            leaven = np.zeros((year2-year1+1))
            for y,year in enumerate(np.arange(year1,year2+1,1)):
                leaven[y] = np.average(dleaven[365*y:365*y+365])

            #cn ratio
            cnratio = np.divide(leavec,leaven)
            #last 20 years of time series 1997-2017
            cnratio9717 = np.average(cnratio)

            #last 20 years of time series 1997-2017
            mosaic[i,j] = cnratio9717 / reference

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=minimum,vmax=maximum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tleave_cn_ratio ({round(tmp_minimum)},{ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_leave_cnratio.png", bbox_inches='tight',orientation = 'landscape')

def recovery_time(minimum=0,maximum=100):

    print("recovery time plotting ...")

    fig = plt.figure(figsize=(25,25))
    MONTHLY_THRESHOLD = 3
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "Recovery score after \n{} months {}-{}".format(MONTHLY_THRESHOLD,year1,year2)
    m = 0
    nb_scenario = 0
    nb_hangover = 0

    i = -1
    for density in tqdm(densitys, total= len(densitys)) : #loop over livestock density i
        i += 1
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### BURNINGDATE
                    ### BURNINGDATE(time,latitude,longitude)
                    nc = Dataset("{}/burningdate.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    burningdates = nc.variables['BURNINGDATE'][y1:y2,0,0]
                    nc.close()

                    ### MONTHLY LEAVE C
                    ### LeafC(time,latitude,longitude)
                    nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = days[y1*365]
                    d2 = days[y2*365-1]
                    dleavecs = nc.variables['LeafC'][d1:d2,0,0] #gC/m2/day
                    nc.close()

                    recovery_score_list = []
                    for id_year,year in enumerate(years[y1:y2]) :

                        if burningdates[id_year] > 0:
                            burn_index = int(id_year*365 + burningdates[id_year])
                            post_burn_index = burn_index+MONTHLY_THRESHOLD*30
                            nb_scenario += 1

                            # calculation pre_burn vegetation status
                            if burn_index-15 >=0: # be sure 15 days are available before
                                pre_burn_status = np.average(dleavecs[burn_index-15:burn_index-1])
                            else :
                                pre_burn_status = np.average(dleavecs[0:burn_index-1])

                            if post_burn_index+7 <= len(dleavecs):
                                post_burn_status = np.average(dleavecs[post_burn_index-7:post_burn_index+7])
                            else :
                                continue

                            recovery_score_list.append(post_burn_status*100/pre_burn_status)

                    mosaic[l,k] = np.average(recovery_score_list)

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}%\n±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"hangover excluded:{nb_hangover}/{nb_scenario}")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_recoverytime.png", bbox_inches='tight',orientation = 'landscape')

def dropout_post_fire():

    print("drop out post fire plotting ...")

    fig = plt.figure(figsize=(25,25))
    MONTHLY_THRESHOLD = 5
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "Post-Fire Drop-Out {}-{}".format(year1,year2)
    m = 0

    for i,density in enumerate(densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### BURNINGDATE
                    ### BURNINGDATE(time,latitude,longitude)
                    nc = Dataset("{}/burningdate.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    burningdates = nc.variables['BURNINGDATE'][y1:y2,0,0] #id day/years
                    nc.close()

                    ### MONTHLY LEAVE C
                    ### LeafC(time,latitude,longitude)
                    nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = days[y1*365]
                    d2 = days[y2*365-1]
                    dleavecs = nc.variables['LeafC'][d1:d2,0,0] #gC/m2/day
                    nc.close()

                    ### DAILY NPP
                    nc = Dataset("{}/d_npp.nc".format(folder), mode='r')
                    npp = nc.variables['NPP'][d1:d2,0,0] #gC/m2/days
                    nc.close()

                    dropout = 0
                    for id_year,year in enumerate(years[y1:y2]) :

                        # if burn event
                        if burningdates[id_year] > 0:
                            burn_index = int(id_year*365 + burningdates[id_year])
                            post_burn_index1 = burn_index+MONTHLY_THRESHOLD*30
                            post_burn_index2 = burn_index+(MONTHLY_THRESHOLD+2)*30

                            if post_burn_index2 <= len(dleavecs): #condition if end of data reached
                                sum_bminc = np.sum(npp[post_burn_index1:post_burn_index2])
                                if sum_bminc < -5 :
                                    dropout += 1
                                    #print(f"{density} {term} {frequency} {date} {year}")

                    mosaic[l,k] = dropout

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.sum(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.sum(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.sum(mosaic[:,7:],axis=1)
            smosaic = smosaic/70*100
            # excluded scenario
            mask = smosaic > 10
            # Convert boolean values to 1s and 0s
            exc_scenario = mask.astype(int)
            print(f'"{biome}_{term}_{density}" : [')
            print(f"[{exc_scenario[0][0]},{exc_scenario[0][1]},{exc_scenario[0][2]}],")
            print(f"[{exc_scenario[1][0]},{exc_scenario[1][1]},{exc_scenario[1][2]}],")
            print(f"[{exc_scenario[2][0]},{exc_scenario[2][1]},{exc_scenario[2][2]}],")
            print(f"[{exc_scenario[3][0]},{exc_scenario[3][1]},{exc_scenario[3][2]}]],")

            # get the mosaic from excluded_scenario
            try :
                imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            except :
                imp_excluded_scenario = exc_scenario
            mosaic_excluded_scenario = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(mosaic_excluded_scenario,cmap="YlOrRd",vmin=0,vmax=10)
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,f"{int(smosaic[l,k])}%",fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_dropout.png", bbox_inches='tight',orientation = 'landscape')

def selected_scenario ():

    print("selected scenario plotting ...")

    fig = plt.figure(figsize=(25,25))
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "Selected Scenario {}-{}".format(year1,year2)
    m = 0

    with open("excluded_scenario.txt", 'r') as file:
        excluded_scenario = file.readlines()
        # Strip newline characters from each line
        excluded_scenario = [line.strip() for line in excluded_scenario]

    for i,density in enumerate(tqdm(densitys, total= len(densitys))) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            txtmosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    if f"{biome}_{density}_{term}_{frequency}_{date}" not in excluded_scenario:
                        mosaic[l,k] = 1

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.sum(mosaic[:,0:2],axis=1)/2
            txtmosaic[:,0] = np.sum(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.sum(mosaic[:,2:7],axis=1)/5
            txtmosaic[:,1] = np.sum(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.sum(mosaic[:,7:],axis=1)/10
            txtmosaic[:,2] = np.sum(mosaic[:,7:],axis=1)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(-1*smosaic,cmap="YlOrRd",vmin=-1,vmax=0)
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,f"{int(txtmosaic[l,k])}",fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_selected_scenario.png", bbox_inches='tight',orientation = 'landscape')

def vegetation_lost (minimum,maximum):

    fig = plt.figure(figsize=(25,25))
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "Vegetation lost {}-{}".format(year1,year2)
    m = 0
    tmp_maximum = None

    print(f"vegetation lost plotting ...")

    i = -1
    for density in tqdm(densitys, total= len(densitys)) : #loop over livestock density i
        i += 1
        rdensity = rdensitys[i]

        for j,term in enumerate(terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate(frequencys) : #loop over frequency k

                for l,date in enumerate(dates): #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### BURNINGDATE
                    ### BURNINGDATE(time,latitude,longitude)

                    nc = Dataset("{}/burningdate.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    burningdates = nc.variables['BURNINGDATE'][y1:y2,0,0]
                    nc.close()

                    ### DAILY LEAVE C
                    ### LeafC(time,latitude,longitude)

                    nc = Dataset("{}/d_cleaf.nc".format(folder), mode='r')
                    days = nc.variables['time'][:]
                    d1 = days[y1*365]
                    d2 = days[y2*365-1]
                    dleavecs = nc.variables['LeafC'][d1:d2,0,0] #gC/m2/yr
                    nc.close()

                    veg_lost = []

                    for id_year,burningdate in enumerate(burningdates):
                        burningdate = int(burningdate)

                        if burningdate > 0:
                            veg_pre_burn = dleavecs[id_year*365+burningdate-1]
                            veg_post_burn = dleavecs[id_year*365+burningdate]
                            veglost = veg_pre_burn - veg_post_burn

                            veg_lost.append(veglost*100/veg_pre_burn)

                    mosaic[l,k] = np.average(veg_lost)

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=minimum,vmax=maximum) #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}\n±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tvegetation lost ({ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_vegetationlost.png", bbox_inches='tight',orientation = 'landscape')

def som_n (minimum,maximum):

    print("som n plotting ...")

    tmp_minimum = None
    tmp_maximum = None

    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "SOM-N {}-{} (gN/m2)".format(year1,year2)

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### SOILN
                    ### SoilN(time,latitude,longitude)
                    nc = Dataset("{}/soiln.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soiln = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
                    nc.close()

                    #average over time period (last 20 years)
                    mosaic[l,k] = np.average(soiln[:])

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{} ±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)
            ### SOILN
            ### SoilN(time,latitude,longitude)
            nc = Dataset("{}/soiln.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soiln = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
            nc.close()

            #last 20 years of time series 1997-2017
            mosaic[i,j] = np.average(soiln[:])

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tsom_n ({round(tmp_minimum)},{ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_som.png", bbox_inches='tight',orientation = 'landscape')

def soil_cn_ratio (minimum,maximum,reference):

    print("soil CN ratio plotting ...")

    tmp_minimum = None
    tmp_maximum = None

    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "Soil CN Ratio {}-{}".format(year1,year2)

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### SOILN
                    ### SoilN(time,latitude,longitude)
                    nc = Dataset("{}/soiln.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soiln = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
                    nc.close()

                    ### SOILC
                    ### SoilC(time,latitude,longitude)
                    nc = Dataset("{}/soilc.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soilC = nc.variables['SoilC'][y1:y2,0,0] #gC/m2/yr
                    nc.close()

                    #average over time period (last 20 years)
                    cnratio = soilC/soiln
                    mosaic[l,k] = np.average(cnratio[:]) / reference

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=minimum,vmax=maximum,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}±{}".format(round(smosaic[l,k],3),round(stdmosaic[l,k],3)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)
            ### SOILN
            ### SoilN(time,latitude,longitude)
            nc = Dataset("{}/soiln.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soiln = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
            nc.close()

            ### SOILC
            ### SoilC(time,latitude,longitude)
            nc = Dataset("{}/soilc.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soilC = nc.variables['SoilC'][y1:y2,0,0] #gC/m2/yr
            nc.close()

            #last 20 years of time series 1997-2017
            cnratio = soilC/soiln
            mosaic[i,j] = np.average(cnratio[:])  / reference

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tsoil cn ratio ({round(tmp_minimum)},{ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_soil_cnratio.png", bbox_inches='tight',orientation = 'landscape')

def som_n_budget (minimum,maximum):

    print("som n budget plotting ...")

    fig = plt.figure(figsize=(25,25))
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "SOM-N budget {}-{} (gN/m2)".format(year1,year2)

    tmp_minimum = None
    tmp_maximum = None

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### SOILN
                    ### SoilN(time,latitude,longitude)
                    nc = Dataset("{}/soiln.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]+1
                    y2 = np.where(years == year2)[0][0]+1
                    soiln1 = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
                    soilnm1 = nc.variables['SoilN'][y1-1:y2-1,0,0] #gN/m2/yr
                    nc.close()

                    som_n = soiln1 - soilnm1
                    mosaic[l,k] = np.average(som_n[:])

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="bwr",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}\n±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")


    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)

            ### SOILN
            ### SoilN(time,latitude,longitude)
            nc = Dataset("{}/soiln.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]+1
            y2 = np.where(years == year2)[0][0]+1
            soiln1 = nc.variables['SoilN'][y1:y2,0,0] #gN/m2/yr
            soilnm1 = nc.variables['SoilN'][y1-1:y2-1,0,0] #gN/m2/yr
            nc.close()

            som_n = soiln1 - soilnm1
            mosaic[i,j] = np.average(som_n[:])

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]+0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="bwr",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    tmp_maximum = max(abs(tmp_minimum),abs(tmp_maximum))
    print(f"suggestion:\t\t\tsom_n_budget ({round(-tmp_maximum,4)},{round(tmp_maximum,4)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')

    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_som_n_budget.png", bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()

def smm_n (minimum,maximum):

    print("smm n plotting ...")
    tmp_minimum = None
    tmp_maximum = None

    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "SMM-N {}-{} (gN/m2)".format(year1,year2)

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### SoilNO3
                    ### SoilNO3(time,latitude,longitude)
                    nc = Dataset("{}/soilno3.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soilno3 = nc.variables['SoilNO3'][y1:y2,0,0] #gN/m2/yr
                    nc.close()

                    ### SoilNH4
                    ### SoilNH4(time,latitude,longitude)
                    nc = Dataset("{}/soilnh4.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soilnh4 = nc.variables['SoilNH4'][y1:y2,0,0] #gN/m2/yr
                    nc.close()

                    #average over time period (last 20 years)
                    smm_n = soilno3 + soilnh4
                    mosaic[l,k] = np.average(smm_n[:])

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{} ±{}".format(round(smosaic[l,k],3),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)

            ### SoilNO3
            ### SoilNO3(time,latitude,longitude)
            nc = Dataset("{}/soilno3.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soilno3 = nc.variables['SoilNO3'][y1:y2,0,0] #gN/m2/yr
            nc.close()

            ### SoilNH4
            ### SoilNH4(time,latitude,longitude)
            nc = Dataset("{}/soilnh4.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soilnh4 = nc.variables['SoilNH4'][y1:y2,0,0] #gN/m2/yr
            nc.close()

            #average over time period (last 20 years)
            smm_n = soilno3 + soilnh4
            mosaic[i,j] = np.average(smm_n[:])

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]+0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],4)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tsmm ({round(tmp_minimum,4)},{round(tmp_maximum,4)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')

    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_smm_n.png", bbox_inches='tight',orientation = 'landscape')

    matplotlib.pyplot.close()

def smm_n_budget (minimum,maximum):

    print("smm n budget plotting ...")
    tmp_minimum = None
    tmp_maximum = None

    fig = plt.figure(figsize=(25,25))
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = "SMM-N Budget {}-{} (gN/m2)".format(year1,year2)

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)
                    ### SoilNO3
                    ### SoilNO3(time,latitude,longitude)
                    nc = Dataset("{}/soilno3.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]+1
                    y2 = np.where(years == year2)[0][0]+1
                    soilno3n1 = nc.variables['SoilNO3'][y1:y2,0,0] #gN/m2/yr
                    soilno3nm1 = nc.variables['SoilNO3'][y1-1:y2-1,0,0] #gN/m2/yr
                    nc.close()

                    ### SoilNH4
                    ### SoilNH4(time,latitude,longitude)
                    nc = Dataset("{}/soilnh4.nc".format(folder), mode='r')
                    soilnh4n1 = nc.variables['SoilNH4'][y1:y2,0,0] #gN/m2/yr
                    soilnh4nm1 = nc.variables['SoilNH4'][y1-1:y2-1,0,0] #gN/m2/yr
                    nc.close()

                    smm_n_n1 = soilno3n1 + soilnh4n1
                    smm_n_nm1 = soilno3nm1 + soilnh4nm1
                    smm_n = smm_n_n1 - smm_n_nm1

                    mosaic[l,k] = np.average(smm_n[:])

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_minimum = test_minimum (tmp_minimum,smosaic)
            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="bwr",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}\n±{}".format(round(smosaic[l,k],3),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)

            ### SoilNO3
            ### SoilNO3(time,latitude,longitude)
            nc = Dataset("{}/soilno3.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]+1
            y2 = np.where(years == year2)[0][0]+1
            soilno3n1 = nc.variables['SoilNO3'][y1:y2,0,0] #gN/m2/yr
            soilno3nm1 = nc.variables['SoilNO3'][y1-1:y2-1,0,0] #gN/m2/yr
            nc.close()

            ### SoilNH4
            ### SoilNH4(time,latitude,longitude)
            nc = Dataset("{}/soilnh4.nc".format(folder), mode='r')
            soilnh4n1 = nc.variables['SoilNH4'][y1:y2,0,0] #gN/m2/yr
            soilnh4nm1 = nc.variables['SoilNH4'][y1-1:y2-1,0,0] #gN/m2/yr
            nc.close()

            #average over time period (last 20 years)
            smm_n_n1 = soilno3n1 + soilnh4n1
            smm_n_nm1 = soilno3nm1 + soilnh4nm1
            smm_n = smm_n_n1 - smm_n_nm1
            mosaic[i,j] = np.average(smm_n[:])

    tmp_minimum = test_minimum (tmp_minimum,mosaic)
    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]+0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="bwr",vmin=-maximum,vmax=-minimum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],3)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    tmp_maximum = max(abs(tmp_minimum),abs(tmp_maximum))
    print(f"suggestion:\t\t\tsom_n_budget ({round(-tmp_maximum,4)},{round(tmp_maximum,4)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')

    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_smm_n_budget.png", bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()

def global_warming_potential (maximum):

    print("global warming potential plotting ...")

    fig = plt.figure(figsize=(25,25))
    subplots = set_subplots(2,4,0.2,0.15,0.01,0.06)
    title = f"SUM Global Warming Potential {year1}-{year2}"

    y1,y2 = year1 - 1948, year2 - 1948
    tmp_maximum = None
    m = 0

    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ###
                    ### FIRE EMISSION - CO2
                    ### co2_emission(time,latitude,longitude)
                    ###
                    nc = Dataset("{}/fireemission_co2.nc".format(folder), mode='r')
                    mfireco2 = nc.variables['co2_emission'][:,0,0] #gCO2/m2/month
                    nc.close()

                    fireco2 = monthly_to_annual(mfireco2, "sum")[y1:y2] #gCO2/m2/year
                    fireco2 = fireco2 * 1e-3 #kgCO2/m2/year
                    annual_global_warming_potential = fireco2

                    ###
                    ### FIRE EMISSION - CH4
                    ### ch4_emission(time,latitude,longitude)
                    ###
                    nc = Dataset("{}/fireemission_ch4.nc".format(folder), mode='r')
                    mfirech4 = nc.variables['ch4_emission'][:,0,0] #gCH4/m2/month
                    nc.close()

                    firech4 = monthly_to_annual(mfirech4, "sum")[y1:y2] #gCH4/m2/year
                    firech4 = firech4 * 1e-3 #kgCH4/m2/year
                    gwp_firech4 = firech4 * 25 #kgCO2e/m2/year
                    annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_firech4)

                    ###
                    ### C EMISSION FROM SOIL
                    ###
                    nc = Dataset("{}/puresoilc.nc".format(folder), mode='r')
                    puresoilc = nc.variables['MGRASS_SOILC'][y1:y2,0,0] #gC/m2/day
                    nc.close()

                    soilemission = np.asarray([0])
                    soilemission = np.append(soilemission,puresoilc[1:] - puresoilc[:-1])
                    soilemission[soilemission < 0] = 0
                    soilemission = soilemission * 1e-3 #kgC/m2/year
                    gwp_soilemission = soilemission * (12+16*2)/12 ##kgCo2e/m2/year
                    annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_soilemission)

                    ###
                    ### N EMISSION FROM DENITRIFICATION AND NITRIFICATION
                    ### denitrification output = sum (denitrification, nitrification)
                    ### N2O_denitrification(time,latitude,longitude)
                    ###
                    nc = Dataset("{}/n2o_denit.nc".format(folder), mode='r')
                    monthly_denitrification = nc.variables['N2O_denitrification'][:,0,0] #gN/m2/month
                    nc.close()

                    denitrification = monthly_to_annual(monthly_denitrification, "sum")[y1:y2] #gCO2/m2/year
                    gwp_denitrification = denitrification * 298 * 44/14 * 1e-3 #kgCo2e/m2/year

                    annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_denitrification)
                    annual_global_warming_potential = ma.masked_where(annual_global_warming_potential == 0, annual_global_warming_potential)

                    mosaic[l,k] = np.nansum(annual_global_warming_potential)

            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=0,vmax=maximum) #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     "{}\nlivestock density {} LSU/ha\n{} term practice".format(title,rdensity,term),
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{}\n±{}".format(round(smosaic[l,k],2),round(stdmosaic[l,k],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")



    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i


        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)

            ###
            ### FIRE EMISSION - CO2
            ### co2_emission(time,latitude,longitude)
            ###
            nc = Dataset("{}/fireemission_co2.nc".format(folder), mode='r')
            mfireco2 = nc.variables['co2_emission'][:,0,0] #gCO2/m2/month
            nc.close()

            fireco2 = monthly_to_annual(mfireco2, "sum")[y1:y2] #gCO2/m2/year
            fireco2 = fireco2 * 1e-3 #kgCO2/m2/year
            annual_global_warming_potential = fireco2

            ###
            ### FIRE EMISSION - CH4
            ### ch4_emission(time,latitude,longitude)
            ###
            nc = Dataset("{}/fireemission_ch4.nc".format(folder), mode='r')
            mfirech4 = nc.variables['ch4_emission'][:,0,0] #gCH4/m2/month
            nc.close()

            firech4 = monthly_to_annual(mfirech4, "sum")[y1:y2] #gCH4/m2/year
            firech4 = firech4 * 1e-3 #kgCH4/m2/year
            gwp_firech4 = firech4 * 25 #kgCO2e/m2/year
            annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_firech4)

            ###
            ### C EMISSION FROM SOIL
            ###
            nc = Dataset("{}/puresoilc.nc".format(folder), mode='r')
            puresoilc = nc.variables['MGRASS_SOILC'][y1:y2,0,0] #gC/m2/day
            nc.close()

            soilemission = np.asarray([0])
            soilemission = np.append(soilemission,puresoilc[1:] - puresoilc[:-1])
            soilemission[soilemission < 0] = 0
            soilemission = soilemission * 1e-3 #kgC/m2/year
            gwp_soilemission = soilemission * (12+16*2)/12 ##kgCo2e/m2/year
            annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_soilemission)

            ###
            ### N EMISSION FROM DENITRIFICATION AND NITRIFICATION
            ### denitrification output = sum (denitrification, nitrification)
            ### N2O_denitrification(time,latitude,longitude)
            ###
            nc = Dataset("{}/n2o_denit.nc".format(folder), mode='r')
            monthly_denitrification = nc.variables['N2O_denitrification'][:,0,0] #gN/m2/month
            nc.close()

            denitrification = monthly_to_annual(monthly_denitrification, "sum")[y1:y2] #gCO2/m2/year
            gwp_denitrification = denitrification * 298 * 44/14 * 1e-3 #kgCo2e/m2/year

            annual_global_warming_potential = np.add(annual_global_warming_potential,gwp_denitrification)
            annual_global_warming_potential = ma.masked_where(annual_global_warming_potential == 0, annual_global_warming_potential)

            mosaic[i,j] = np.nansum(annual_global_warming_potential)

    tmp_maximum = test_maximum (tmp_maximum,mosaic)
    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]+0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic,cmap="YlOrRd",vmin=0,vmax=maximum) #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tglobal warming potential ({ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')

    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_gwp.png", bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()

def soil_c (maximum):

    print("soil c plotting ...")
    fig = plt.figure(figsize=(25,30))
    subplots = set_subplots(2,4,0.12,0.20,0.01,0.04)
    title = "SoilC (gC/m2)"
    tmp_maximum = None

    m = 0
    for i,density in enumerate (densitys) : #loop over livestock density i
        rdensity = rdensitys[i]

        for j,term in enumerate (terms) : # loop over terms j

            mosaic = np.zeros((len(dates),len(frequencys))) #mosaic with all frequencies and burning years
            smosaic = np.zeros((len(dates),3)) #short mosaic / average over frequencies
            stdmosaic = np.zeros((len(dates),3)) #standard deviation mosaic

            for k,frequency in enumerate (frequencys) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l

                    folder = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(path,pixel,term,density,frequency[0],frequency[1],date)

                    ### SOILC
                    ### SoilC(time,latitude,longitude)
                    nc = Dataset("{}/soilc.nc".format(folder), mode='r')
                    years = nc.variables['time'][:]
                    y1 = np.where(years == year1)[0][0]
                    y2 = np.where(years == year2)[0][0]+1
                    soilC = nc.variables['SoilC'][y1:y2,0,0] #gC/m2/yr
                    nc.close()

                    #average over time period (last 20 years)
                    mosaic[l,k] = np.average(soilC[:])


            #average over frequency
            #frequence 2
            smosaic[:,0] = np.average(mosaic[:,0:2],axis=1)
            stdmosaic[:,0] = np.std(mosaic[:,0:2],axis=1)
            #frequence 5
            smosaic[:,1] = np.average(mosaic[:,2:7],axis=1)
            stdmosaic[:,1] = np.std(mosaic[:,2:7],axis=1)
            #frequence 10
            smosaic[:,2] = np.average(mosaic[:,7:],axis=1)
            stdmosaic[:,2] = np.std(mosaic[:,7:],axis=1)

            tmp_maximum = test_maximum (tmp_maximum,smosaic)
            # get the mosaic from excluded_scenario
            imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_{term}_{density}"])
            limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, smosaic)

            axe0 = fig.add_axes(subplots[m])
            axe=axe0.twiny()
            im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=0,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
            axe.text(subplots[m][0]+subplots[m][2],subplots[m][1]-0.003,
                     f"{title} - {term} term practice\nlivestock density {rdensity} LSU/ha\n",
                     fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

            #y axis - burning date
            if term == "short":
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels(["early season","late season", "end season","early spring"])
                axe0.set_ylabel("burning date",size=fs_axistitle)
            else :
                axe.set_yticks(np.arange(0,4,1))
                axe.set_yticklabels([])

            #x axis - frequency
            axe.set_xticks(np.arange(0,3,1))
            axe.set_xticklabels([2,5,10])
            axe.set_xlabel("burning frequency",size=fs_axistitle)
            axe0.set_xticks([])
            m+=1

            for k,frequency in enumerate (np.arange(0,3,1)) : #loop over frequency k
                for l,date in enumerate (dates) : #loop over date l
                    axe.text(k,l,"{} ± {}".format(int(smosaic[l,k]),int(stdmosaic[l,k])),fontsize = fs_textongraph,ha="center",va="center",weight="bold")


    #no fire mosaic
    mosaic = np.zeros((len(densitys),len(terms)))

    for i,density in enumerate (densitys) : #loop over livestock density i

        for j,term in enumerate (terms) : # loop over terms j

            folder = "{}{}_{}term_ld{}_nofire".format(path,pixel,term,density)


            ### SOILC
            ### SoilC(time,latitude,longitude)
            nc = Dataset("{}/soilc.nc".format(folder), mode='r')
            years = nc.variables['time'][:]
            y1 = np.where(years == year1)[0][0]
            y2 = np.where(years == year2)[0][0]+1
            soilC = nc.variables['SoilC'][y1:y2,0,0] #gC/m2/yr
            nc.close()

            #last 20 years of time series 1997-2017
            mosaic[i,j] = np.average(soilC[:])


            tmp_maximum = test_maximum (tmp_maximum,mosaic)

    # get the mosaic from excluded_scenario
    imp_excluded_scenario = np.array(excluded_scenario[f"{biome}_nofire"])
    limitmosaic = ma.masked_where(imp_excluded_scenario > 0.001, mosaic)

    subplots = set_subplots(1,1,0.15,0.1,0.01,0.05)
    subplots[0][1] = subplots[0][1]-0.04
    axe0 = fig.add_axes(subplots[0])
    axe=axe0.twiny()
    im = axe.imshow(limitmosaic*(-1),cmap="YlOrRd",vmin=-maximum,vmax=0,aspect='auto') #more impact = smaler values => *(-1) + inversion min and max
    axe.text(subplots[0][0]+subplots[0][2],subplots[0][1]-0.003,
        "{}\nNo burning practice".format(title),
        fontsize = fs_title,ha="right",va="top",weight="bold",transform=fig.transFigure)

    #y axis - livestock density
    axe.set_yticks(np.arange(0,3,1))
    axe.set_yticklabels([0,0.1,0.5])
    axe0.set_ylabel("livestock density",size=fs_axistitle)

    #x axis - practice term
    axe.set_xticks(np.arange(0,2,1))
    axe.set_xticklabels(["short","long"])
    axe.set_xlabel("term",size=fs_axistitle)
    axe0.set_xticks([])

    for i,density in enumerate (densitys) :
        for j,term in enumerate (terms) :
            axe.text(j,i,"{}".format(round(mosaic[i,j],2)),fontsize = fs_textongraph,ha="center",va="center",weight="bold")

    print(f"suggestion:\t\t\tsoil_c ({ceil(tmp_maximum)})")
    pp.savefig(bbox_inches='tight',orientation = 'landscape')
    matplotlib.pyplot.close()
    fig.suptitle(biome, fontsize=16,y=0,x=0)
    fig.savefig(f"{figure_path}_soilc.png", bbox_inches='tight',orientation = 'landscape')

class biome_mosaic:
    def __init__(self):
        pass

    def amazon1_mosaic (self): #done
        harvest_c() # already in
        #leave_c(194.0) # only maximum, minimum = 0
        leave_cn_ratio(1,1.82, 11.42)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(256,529.0)
        #soil_cn_ratio (16,18.0)
        #som_n_budget(-1.9428,1.9428)
        #smm_n(0.0686,1.0384)
        #smm_n_budget(-0.032,0.032)
        #global_warming_potential(35.0) # only maximum, minimum = 0

    def amazon2_mosaic (self): #done
        harvest_c() # already in
        #leave_c(181.0) # only maximum, minimum = 0
        leave_cn_ratio(1,1.82,11.29)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(219,516.0)
        #soil_cn_ratio (16,18.0)
        #som_n_budget(-2.8529,2.8529)
        #smm_n(0.0991,2.0691)
        #smm_n_budget(-0.0359,0.0359)
        #global_warming_potential(19) # only maximum, minimum = 0

    def caatinga_mosaic (self): #done
        harvest_c() # already in
        #leave_c(56.0) # only maximum, minimum = 0
        leave_cn_ratio(1,1.82,12.22)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100)# only maximum, minimum = 0 # error
        #som_n(86,190.0)
        #soil_cn_ratio (17,21.0)
        #som_n_budget(-0.9842,0.9842)
        #smm_n(0.2311,4.6316)
        #smm_n_budget(-0.052,0.052)
        #global_warming_potential(7.0) # only maximum, minimum = 0

    def cerrado_mosaic (self):
        harvest_c() # already in
        leave_c(70) #leave_c(144) # only maximum, minimum = 0
        leave_cn_ratio(1,1.7,14.31)
        recovery_time(0,178) # already in, max >= 2 years
        dropout_post_fire()
        vegetation_lost(0,100) # only maximum, minimum = 0
        som_n(200,360)
        soil_cn_ratio (0.98,1.07,16.65)
        som_n_budget(-2.0495,2.0495)
        smm_n(0.15,1.28)
        smm_n_budget(-0.0277,0.0277)
        global_warming_potential(18) # only maximum, minimum = 0
        soil_c(5764)

    def mata1_mosaic (self):
        harvest_c() # already in
        #leave_c(234.0) # only maximum, minimum = 0
        leave_cn_ratio(1,1.82,18.44)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(261,626.0)
        #soil_cn_ratio (17,19.0)
        #som_n_budget(-3.1676,3.1676)
        #smm_n(0.0256,0.338)
        #smm_n_budget(-0.0087,0.0087)
        #global_warming_potential(35) # only maximum, minimum = 0)

    def mata2_mosaic (self):
        harvest_c() # already in
        #leave_c(300.0)#leave_c(348.0) # only maximum, minimum = 0
        leave_cn_ratio(1,3,24.4)
        #recovery_time(69,87) # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(786,1108.0)
        #soil_cn_ratio (17,18.0)
        #som_n_budget(-3.7166,3.7166)
        #smm_n(0.145,0.278)
        #smm_n_budget(-0.0017,0.0017)
        #global_warming_potential(49) # only maximum, minimum = 0
    def pampas_mosaic (self):
        harvest_c() # already in
        #leave_c(330.0) # only maximum, minimum = 0
        leave_cn_ratio(1,1.82,19.73)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(326,884.0)
        #soil_cn_ratio (17,18.0)
        #som_n_budget(-3.8994,3.8994)
        #smm_n(0.1284,2.0821)
        #smm_n_budget(-0.0133,0.0133)
        #global_warming_potential(36) # only maximum, minimum = 0
        #soil_c(13643)

    def pantanal_mosaic (self):
        #harvest_c() # already in
        #leave_c(141.0) # only maximum, minimum = 0
        leave_cn_ratio(12,31.0)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost(0,100) # only maximum, minimum = 0
        #som_n(194,417.0)
        #soil_cn_ratio (16,18.0)
        #som_n_budget(-2.4101,2.4101)
        #smm_n(0.1657,3.787)
        #smm_n_budget(-0.0877,0.0877)
        #global_warming_potential(15) # only maximum, minimum = 0

    def mata2bis_mosaic (self):
        harvest_c() # already in
        #leave_c(357.0) # only maximum, minimum = 0
        #leave_cn_ratio(24,26.0)
        #recovery_time() # already in, max >= 2 years
        #dropout_post_fire()
        #vegetation_lost (0,100) # only maximum, minimum = 0
        #som_n (840,1235.0)
        #soil_cn_ratio (17,18.0)
        #som_n_budget(-2.5694,2.5694)
        #smm_n(0.1496,0.2674)
        #smm_n_budget(-0.002,0.002)
        #global_warming_potential(51.0) # only maximum, minimum = 0



biome_list = ['amazon1','amazon2','caatinga','cerrado',
              'mata1','mata2','pampas','pantanal']

biome_list = ['cerrado']

for id_biome,biome in enumerate(biome_list):

    print()
    print()
    print("____________________________________________")
    print(biome)
    print("____________________________________________")

    densitys = ["0","01","05"]
    rdensitys = [0,0.1,0.5]
    terms = ["short", "long"]
    frequencys = [[2,0], [2,1],
                  [5,0], [5,1], [5,2], [5,3], [5,4],
                  [10,0], [10,1], [10,2], [10,3], [10,4], [10,5], [10,6], [10,7], [10,8], [10,9]] #frequency !! include start year in
    dates = [1,2,3,4] #burning date strategy
    burningdates = ["early season","late season", "end season","early spring"]

    pixel = get_biome_pixel(biome)
    path = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(biome,pixel)
    figure_path = f"figures_output/mosaic_png"
    figure_path = f"{figure_path}/{biome}_mosaic"

    year1 = 1948
    year2 = 2017
    day1 = (year1 - 1948)*365
    day2 = (year2 - 1948)*365

    pp = PdfPages(f"figures_output/{biome}_mosaic_{year1}_{year2}.pdf")
    getattr(globals()['biome_mosaic'](), f'{biome}_mosaic')()
    pp.close()
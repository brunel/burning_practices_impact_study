#libraries to import
from pylab import *
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "mata2"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "short" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] = 2 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 2 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']
run_config['compa'] = False

# a) run configuration
run2_config = dict()
run2_config['biome'] = "cerrado"
run2_config['pixel'] = sf.get_biome_pixel (run2_config['biome'])
run2_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run2_config['biome'],run2_config['pixel'])
run2_config['practice_term'] = "long" # short/long
run2_config['livestock_density'] = "01" # 0/01/05
run2_config['burning_frequency'] = 5 # 0/2/5/10
run2_config['burning_phase'] = 0 # [0,burning_frequency,1]
run2_config['burning_date'] = 1 # 1/2/3/4
run2_config['start_year'] = 1948
run2_config['end_year'] = 2017
run2_config['nb_of_year'] = run2_config['end_year'] - run2_config['start_year']
run2_config['scaling'] = True # True/False
if run2_config['burning_frequency'] == 0 :
    run2_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run2_config['run_collection_path'],run2_config['pixel'],run2_config['practice_term'],run2_config['livestock_density'])
else :
    run2_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run2_config['run_collection_path'],run2_config['pixel'],run2_config['practice_term'],run2_config['livestock_density'],
    run2_config['burning_frequency'],run2_config['burning_phase'],run2_config['burning_date'])
run2_config['nb_year'] = run2_config['end_year'] - run2_config['start_year']
run_config['run2_config'] = run2_config


# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 1948 # 99 = no el nino
plot_config['end_year'] = 2017
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "plot_overview"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11

# todo : time scaling = annual, + x axis years + store figures production + when new version: black + structure
# POOL ________________________________________________
# CARBON

#sf.plot_leavec(run_config,plot_config)
#sf.plot_litter_c (run_config,plot_config)
#sf.plot_soil_c (run_config,plot_config)
#sf.plot_npp(run_config,plot_config)

# NITROGEN
#sf.plot_leaven(run_config,plot_config)
#sf.plot_litter_n (run_config,plot_config)
#sf.plot_soil_n (run_config,plot_config)
#sf.plot_smm_n(run_config,plot_config)

# CN RATIO
#sf.plot_leave_cnratio(run_config,plot_config)
#sf.plot_lit_cnratio(run_config,plot_config)
#sf.plot_soil_cnratio(run_config,plot_config)

# FLUXES ________________________________________________
# CARBON
#sf.plot_harvestc(run_config,plot_config)

# NITROGEN
#sf.plot_manure (run_config,plot_config)
#sf.plot_litterfall_n (run_config,plot_config)
#sf.plot_immobilisation(run_config,plot_config)
#sf.plot_mineralisation (run_config,plot_config)

#sf.plot_budgetSOM (run_config,plot_config)
#sf.plot_budgetSMM (run_config,plot_config)
#sf.plot_uptake (run_config,plot_config)
#sf.plot_denitrification (run_config,plot_config)

# BUDGET ________________________________________________
#sf.plot_som_budget_ts(run_config,plot_config)
#sf.plot_smm_budget_ts(run_config,plot_config)

# SOIL FEATURE ________________________________________________
#sf.plot_soiltemp(run_config,plot_config)
#sf.plot_soil_water_content (run_config,plot_config)
#sf.plot_litter_moisture(run_config,plot_config) #cme

# CLIMATE CONDITIONS ________________________________________________
#sf.plot_precipitation(run_config,plot_config)
#sf.plot_temperature (-13.75,-48.75,plot_config,run_config) ## becarfull with this !!! # cerrado = -13.75	-48.75
#sf.plot_seasonality(run_config,plot_config)

#sf.plot_burningdate(run_config,plot_config)

# FIRE EMISSIONS ________________________________________________
#sf.fireemissions_co2e (run_config,plot_config)

# OLD VERSION

#sf.plot_recovery_time(1998,run_config,plot_config)
#sf.plot_co2_from_fire (run_config,plot_config)
#sf.plot_ch4_from_fire (run_config,plot_config)




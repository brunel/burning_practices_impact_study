# 1. imports and global variables
import numpy as np
from netCDF4 import *
import sys
import matplotlib.lines as mtplines
import matplotlib.pyplot as plt
import pandas as pd
import numpy.ma as ma


CUMDAY = [0, 31, 61, 92, 122, 153, 183, 214, 245, 275, 306, 336, 365]

# 2. function definitions

# a) dictionnary definition___________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________

def output_dictionnary(run_config):
    output_collection = dict()

    # 1. precipitation
    precipitation = dict()
    precipitation['denomination'] = "daily precipitation"
    precipitation['file_path'] = "{}/listing_weather_data.out".format(run_config['run_collection_path'])
    precipitation['color'] = "#0064c7"
    precipitation['y_axis_label'] = "precipitation (mm)"
    precipitation['unit'] = "mm/day"
    output_collection[precipitation['denomination']] = precipitation

    # 1bis. temperature
    temperature = dict()
    temperature['denomination'] = "daily temperature"
    temperature['file_path'] = "temperature.nc"
    temperature['color'] = "#A52A2A"
    temperature['y_axis_label'] = "temperature (°C)"
    temperature['variable'] = 'temp'
    temperature['unit'] = "mm/day"
    output_collection[temperature['denomination']] = temperature

    # 2. fire emission - Co2
    fire_emission_co2 = dict()
    fire_emission_co2['denomination'] = "monthly co2 emission from fire"
    fire_emission_co2['file_path'] = "{}/fireemission_co2.nc".format(run_config['folder_path'])
    fire_emission_co2['unit'] = "gCO2/m2/month"
    fire_emission_co2['variable'] = 'co2_emission'
    fire_emission_co2['color'] = '#8c2d04'
    fire_emission_co2['label'] = 'Co2'
    output_collection[fire_emission_co2['denomination']] = fire_emission_co2

    # 3. fire emission - CH4
    fire_emission_ch4 = dict()
    fire_emission_ch4['denomination'] = "monthly ch4 emission from fire"
    fire_emission_ch4['file_path'] = "{}/fireemission_ch4.nc".format(run_config['folder_path'])
    fire_emission_ch4['unit'] = "gCH4/m2/month"
    fire_emission_ch4['variable'] = 'ch4_emission'
    fire_emission_ch4['color'] = '#fe9929'
    fire_emission_ch4['label'] = 'CH4'
    output_collection[fire_emission_ch4['denomination']] = fire_emission_ch4

    # 4. soil c
    soil_c = dict()
    soil_c['denomination'] = "annual soil c"
    soil_c['file_path'] = "{}/soilc.nc".format(run_config['folder_path'])
    soil_c['unit'] = "gC/m2/year"
    soil_c['variable'] = 'SoilC'
    output_collection[soil_c['denomination']] = soil_c

    # 5. n emission from soil : nitrification and denitrification
    soil_n_emission = dict()
    soil_n_emission['denomination'] = "monthly n emission from soil"
    soil_n_emission['file_path'] = "{}/n2o_denit.nc".format(run_config['folder_path'])  # in denit output, nit + denit
    soil_n_emission['unit'] = "gN/m2/month"
    soil_n_emission['variable'] = 'N2O_denitrification'
    soil_n_emission['color'] = '#f768a1'
    soil_n_emission['label'] = 'Soil N'
    output_collection[soil_n_emission['denomination']] = soil_n_emission

    # 6. gwp
    gwp_emission = dict()
    gwp_emission['denomination'] = "annual gwp"
    gwp_emission['unit'] = "kgCo2/m2/year"
    gwp_emission['color'] = "#bdbdbd"
    gwp_emission['y_axis_label'] = "GHG emission (Co2e)"
    output_collection[gwp_emission['denomination']] = gwp_emission

    # 7. Leaf C
    leave_c = dict()
    leave_c['denomination'] = "daily leave carbon"
    leave_c['file_path'] = "{}/d_cleaf.nc".format(run_config['folder_path'])
    leave_c['color'] = "#000000"
    leave_c['y_axis_label'] = "leaf C (gC/m2)"
    leave_c['unit'] = "gC/m2/day"
    leave_c['variable'] = 'LeafC'
    output_collection[leave_c['denomination']] = leave_c

    # 8. daily soil c
    soil_c = dict()
    soil_c['denomination'] = "daily soil c"
    soil_c['file_path'] = "{}/d_mgrass_soilc.nc".format(run_config['folder_path'])
    soil_c['unit'] = "gC/m2/day"
    soil_c['variable'] = 'd_mgrass_soilc'
    soil_c['color'] = '#7a0177'
    soil_c['label'] = 'Soil C'
    output_collection[soil_c['denomination']] = soil_c

    # 9. Harvest C
    harvest_c = dict()
    harvest_c['denomination'] = "daily harvest carbon"
    harvest_c['file_path'] = "{}/d_mgrass_harvestc.nc".format(run_config['folder_path'])
    harvest_c['color'] = "#000000"
    harvest_c['y_axis_label'] = "harvest C (gC/m2)"
    harvest_c['unit'] = "gC/m2/day"
    harvest_c['variable'] = 'd_mgrass_harvestc'
    output_collection[harvest_c['denomination']] = harvest_c

    # 10. Leave N
    leave_n = dict()
    leave_n['denomination'] = "daily leave nitrogen"
    leave_n['file_path'] = "{}/d_nleaf.nc".format(run_config['folder_path'])
    leave_n['color'] = "#000000"
    leave_n['y_axis_label'] = "leaf N (gN/m2)"
    leave_n['unit'] = "gN/m2/day"
    leave_n['variable'] = 'nleaf'
    output_collection[leave_n['denomination']] = leave_n

    # 11. Leave CN Ratio
    cn_ratio = dict()
    cn_ratio['denomination'] = "leave cn ratio"
    cn_ratio['color'] = "#000000"
    cn_ratio['y_axis_label'] = "Leaf CN ratio"
    output_collection[cn_ratio['denomination']] = cn_ratio

    # 12. Litterfall N
    litterfall_n = dict()
    litterfall_n['denomination']="daily litterfall n"
    litterfall_n['file_path'] = "{}/d_mgrass_litfalln.nc".format(run_config['folder_path'])
    litterfall_n['color'] = "#fe9929"
    litterfall_n['y_axis_label'] = "litterfall N (gN/m2)"
    litterfall_n['unit'] = "gN/m2/day"
    litterfall_n['variable'] = 'd_mgrass_litfalln'
    output_collection[litterfall_n['denomination']] = litterfall_n

    # 12. Litterfall N
    litterfall_n = dict()
    litterfall_n['denomination']="annual litterfall n"
    litterfall_n['file_path'] = "{}/litfalln.nc".format(run_config['folder_path'])
    litterfall_n['color'] = "#fe9929"
    litterfall_n['y_axis_label'] = "litterfall N (gN/m2)"
    litterfall_n['unit'] = "gN/m2/yr"
    litterfall_n['variable'] = 'litfalln'
    output_collection[litterfall_n['denomination']] = litterfall_n

    # 13. Manure from Harvest N
    manure = dict()
    manure['denomination'] = "manure"
    manure['color'] = "#8c2d04"
    output_collection[manure['denomination']] = manure

    # 14. Immobilisation
    immobilisation = dict()
    immobilisation['denomination'] = "monthly immobilisation"
    immobilisation['file_path'] = "{}/n_immobilization.nc".format(run_config['folder_path'])
    immobilisation['color'] = "#f768a1"
    immobilisation['y_axis_label'] = "immobilisation (gN/m2)"
    immobilisation['unit'] = "gN/m2/month"
    immobilisation['variable'] = 'N_immobilization'
    output_collection[immobilisation['denomination']] = immobilisation

    # 15. Mineralisation
    mineralisation = dict()
    mineralisation['denomination'] = "monthly mineralisation"
    mineralisation['file_path'] = "{}/n_mineralization.nc".format(run_config['folder_path'])
    mineralisation['color'] = "#7a0177"
    mineralisation['y_axis_label'] = "mineralisation (gN/m2)"
    mineralisation['unit'] = "gN/m2/month"
    mineralisation['variable'] = 'N_mineralization'
    output_collection[mineralisation['denomination']] = mineralisation

    # 16. Harvest N
    harvestn = dict()
    harvestn['denomination'] = "daily harvest n"
    harvestn['file_path'] = "{}/d_mgrass_harvestn.nc".format(run_config['folder_path'])
    harvestn['color'] = "#000000"
    harvestn['y_axis_label'] = "harvest n (gN/m2)"
    harvestn['unit'] = "gN/m2/daily"
    harvestn['variable'] = 'd_mgrass_harvestn'
    output_collection[harvestn['denomination']] = harvestn

    # 16. Harvest N
    harvestn = dict()
    harvestn['denomination'] = "annual harvest n"
    harvestn['file_path'] = "{}/pft_harvestn.nc".format(run_config['folder_path'])
    harvestn['color'] = "#000000"
    harvestn['y_axis_label'] = "harvest n (gN/m2)"
    harvestn['unit'] = "gN/m2/yr"
    harvestn['variable'] = 'harvestn'
    output_collection[harvestn['denomination']] = harvestn

    # 17. BNF
    bnf = dict()
    bnf['denomination'] = "monthly bnf"
    bnf['file_path'] = "{}/bnf.nc".format(run_config['folder_path'])
    bnf['color'] = "#00d2fe"
    bnf['y_axis_label'] = "bnf (gN/m2)"
    bnf['unit'] = "gN/m2/month"
    bnf['variable'] = 'bnf'
    output_collection[bnf['denomination']] = bnf

    # 18. Deposition
    deposition =dict()
    deposition['denomination'] = "daily deposition"
    deposition['file_path'] = "C:/Users/brune\Documents\local_data_cluster\cerrado_7225/no3_nh4_daily_deposition_7225.out" #input ~ similar in each location
    deposition['color'] = "#31a354"
    deposition['y_axis_label'] = "deposition (gN/m2)"
    deposition['unit'] = "gN/m2/day"
    output_collection[deposition['denomination']] = deposition

    # 19. Fire N
    firen = dict()
    firen['denomination'] = "annual firen"
    firen['file_path'] = "{}/firen.nc".format(run_config['folder_path'])
    firen['color'] = "#9d610c"
    firen['y_axis_label'] = "fire N (gN/m2)"
    firen['unit'] = "gN/m2/years"
    firen['variable'] = 'FireN'
    output_collection[firen['denomination']] = firen

    # 20. Burning Date
    burningdate = dict()
    burningdate['denomination'] = "annual burning date"
    burningdate['file_path'] = "{}/burningdate.nc".format(run_config['folder_path'])
    burningdate['color'] = "#000000"
    burningdate['y_axis_label'] = "Burning Date (day index)"
    burningdate['unit'] = "index/years"
    burningdate['variable'] = 'BURNINGDATE'
    output_collection[burningdate['denomination']] = burningdate

    # 21. Uptake
    uptake = dict()
    uptake['denomination'] = "monthly uptake"
    uptake['file_path'] = "{}/nuptake.nc".format(run_config['folder_path'])
    uptake['color'] = "#f03b20"
    uptake['y_axis_label'] = "uptake (gN/m2)"
    uptake['unit'] = "gN/m2/month"
    uptake['variable'] = 'N_uptake'
    output_collection[uptake['denomination']] = uptake

    # 22. Denitrification
    denitrification = dict()
    denitrification['denomination'] = "monthly denitrification"
    denitrification['file_path'] = "{}/n2o_denit.nc".format(run_config['folder_path'])
    denitrification['color'] = "#0064c7"
    denitrification['y_axis_label'] = "denitrification (gN/m2)"
    denitrification['unit'] = "gN/m2/month"
    denitrification['variable'] = 'N2O_denitrification'
    output_collection[denitrification['denomination']] = denitrification

    # 23. Leaching
    leaching = dict()
    leaching['denomination'] = "monthly leaching"
    leaching['file_path'] = "{}/leaching.nc".format(run_config['folder_path'])
    leaching['color'] = "#b348af"
    leaching['y_axis_label'] = "leaching (gN/m2)"
    leaching['unit'] = "gN/m2/month"
    leaching['variable'] = 'N_leaching'
    output_collection[leaching['denomination']] = leaching

    # 24. Volatisation
    volatisation = dict()
    volatisation['denomination'] = "monthly volatisation"
    volatisation['file_path'] = "{}/n_volatilization.nc".format(run_config['folder_path'])
    volatisation['color'] = "#5ab4ac"
    volatisation['y_axis_label'] = "volatisation (gN/m2)"
    volatisation['unit'] = "gN/m2/month"
    volatisation['variable'] = 'N_volatilization'
    output_collection[volatisation['denomination']] = volatisation

    # 25. Burning Months
    burningmonth = dict()
    burningmonth['denomination'] = "monthly burning"
    burningmonth['color'] = "#a50f15"
    burningmonth['unit'] = "bool/month"
    burningmonth['label'] = "burning month"
    burningmonth['minimum'] = -100
    burningmonth['maximum'] = 100
    output_collection[burningmonth['denomination']] = burningmonth

    # 25. Yearly PFT Harvest C
    harvestc = dict()
    harvestc['denomination'] = "annual harvestc"
    harvestc['color'] = "#000000"
    harvestc['unit'] = "gC/m2/yr"
    harvestc['label'] = "annual harvest C"
    harvestc['minimum'] = ""
    harvestc['maximum'] = ""
    output_collection[harvestc['denomination']] = harvestc

    # 26. Monthly Precipitation
    precipitation = dict()
    precipitation['denomination'] = "monthly precipitation"
    precipitation['file_path'] = "{}/mprec.nc".format(run_config['folder_path'])
    precipitation['variable'] = 'prec'
    precipitation['color'] = "#0064c7"
    precipitation['unit'] = "mm/yr"
    precipitation['label'] = "monthly precipitation"
    precipitation['minimum'] = ""
    precipitation['maximum'] = ""
    precipitation['y_axis_label'] = "precipitation (mm)"
    output_collection[precipitation['denomination']] = precipitation

    # 26. Evap
    evap = dict()
    evap['denomination'] = "monthly evap"
    evap['file_path'] = "{}/mevap.nc".format(run_config['folder_path'])
    evap['variable'] = 'evap'
    evap['color'] = "#0064c7"
    evap['unit'] = "mm/yr"
    evap['label'] = "monthly evap"
    evap['minimum'] = ""
    evap['maximum'] = ""
    evap['y_axis_label'] = "evap (-)"
    output_collection[evap['denomination']] = evap

    # 26. fire emission nox
    nox = dict()
    nox['denomination'] = "nox emission"
    nox['file_path'] = "{}/fireemission_nox.nc".format(run_config['folder_path'])
    nox['variable'] = 'nox_emission'
    nox['color'] = "#0064c7"
    nox['unit'] = "gN/months"
    nox['label'] = "nox emission"
    nox['minimum'] = ""
    nox['maximum'] = ""
    output_collection[nox['denomination']] = nox


    return (output_collection)


# b) data importation ________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________

def import_daily_data(output_dict, run_config, plot_config, extraction_days=None):
    nc = Dataset(output_dict['file_path'], mode='r')
    day_list = nc.variables['time'][:]
    if extraction_days == None:
        first_day = day_list[(plot_config['start_year'] - run_config['start_year']) * 365]
        last_day = day_list[(plot_config['end_year'] - run_config['start_year']) * 365]
        id_first_day = np.where(day_list == first_day)[0][0]
        id_last_day = np.where(day_list == last_day)[0][0]
        output_dict['output_array'] = nc.variables[output_dict['variable']][id_first_day:id_last_day, 0, 0]
    else:
        output_dict['output_array'] = nc.variables[output_dict['variable']][extraction_days[0]:extraction_days[-1], 0,0]
    nc.close()

    return output_dict


def import_monthly_data(output_dict, run_config, plot_config):
    nc = Dataset(output_dict['file_path'], mode='r')
    month_list = nc.variables['time'][:]
    first_month = month_list[(plot_config['start_year'] - run_config['start_year']) * 12]
    last_month = month_list[(plot_config['end_year'] - run_config['start_year']) * 12]
    id_first_month = np.where(month_list == first_month)[0][0]
    id_last_month = np.where(month_list == last_month)[0][0]
    output_dict['output_array'] = nc.variables[output_dict['variable']][id_first_month:id_last_month, 0, 0]
    nc.close()

    return output_dict


def import_annual_data(output_dict, run_config, plot_config, extraction_years=None):
    nc = Dataset(output_dict['file_path'], mode='r')
    if extraction_years == None:
        output_dict['output_array'] = nc.variables[output_dict['variable']][
                                      plot_config['start_year'] - run_config['start_year']:plot_config['end_year'] -
                                                                                           run_config['start_year'], 0,
                                      0]
    else:
        output_dict['output_array'] = nc.variables[output_dict['variable']][extraction_years[0]:extraction_years[1], 0,
                                      0]

    return output_dict

def import_annual_pft_data(output_dict, run_config, plot_config, extraction_years=None):

    nc = Dataset(output_dict['file_path'], mode='r')
    id_pasture = np.where(chartostring(nc.variables['NamePFT'][:])=="pasture")[0][0]

    if extraction_years == None:
        output_dict['output_array'] = nc.variables[output_dict['variable']][
                                      plot_config['start_year'] - run_config['start_year']:plot_config['end_year'] - run_config['start_year'],
                                      id_pasture, 0, 0]
    else:
        output_dict['output_array'] = nc.variables[output_dict['variable']][extraction_years[0]:extraction_years[1],
                                      id_pasture, 0, 0]

    return output_dict


def extracting_daily_output_from_listing(output, file_path, plot_config):
    start_year = plot_config['start_year']
    end_year = plot_config['end_year']

    # opening and extracting line from file
    file = open(file_path, "r")
    lines = file.readlines()
    file.close()

    # initilisation daily_output array
    daily_output = np.zeros(((end_year - start_year) * 365))

    # loop over line
    for line in lines:
        line = line.split(";")
        if output == 'precipitation':
            if line[0] == "daily_grassland_precipitation":
                year, day = int(line[1]), int(line[2]) - 1
                if year >= start_year and year < end_year:
                    daily_output[day + (year - start_year) * 365] = float(line[3])

        if output == 'deposition':
            if 'update_daily_deposition' in line[0] :

                year, day = int(line[1]), int(line[2]) - 1
                if year >= start_year and year < end_year:
                    daily_output[day + (year - start_year) * 365] += float(line[3])

    return (daily_output)

def get_biome_pixel (biome):
    """
    return pixel as an integer from biome (str)
    """
    if biome=="cerrado":
        return 7225
    elif biome == "mata2":
        return 3544
    elif biome == "amazon1":
        return 10674
    elif biome == "amazon2":
        return 9534
    elif biome == "caatinga":
        return 9239
    elif biome == "mata1":
        return 5115
    elif biome == "pampas":
        return 1866
    elif biome == "pantanal":
        return 5743
    elif biome == "mata2bis":
        return 3543
    else :
        print(f"ERROR: pixel for {biome} not implemented")
        exit()

# c) data processing__________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________

def moving_average(data, window_size):
    """
    Calculate the moving average of a time series data.

    Parameters:
        data (list or pandas.Series): Time series data.
        window_size (int): Size of the moving window.

    Returns:
        pandas.Series: Series containing the moving averages.
    """
    if not isinstance(data, pd.Series):
        data = pd.Series(data)

    return data.rolling(window=window_size).mean()


def daily_to_monthly(daily_data, function):
    nb_years = round(len(daily_data) / 365)

    day_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    nb_day_in_months = day_in_months
    for year in range(nb_years - 1):
        nb_day_in_months = day_in_months + nb_day_in_months
    id_last_day_months = (np.cumsum(nb_day_in_months) - 1)

    monthly_data = np.zeros((len(id_last_day_months)))
    for j, last_day in enumerate(id_last_day_months):
        if function == "sum":
            if j == 0:
                monthly_data[j] = np.nansum(daily_data[0:last_day])
            else:
                monthly_data[j] = np.nansum(daily_data[id_last_day_months[j - 1] + 1:last_day])
        elif function == "mean":
            if j == 0:
                monthly_data[j] = np.nanmean(daily_data[0:last_day])
            else:
                monthly_data[j] = np.nanmean(daily_data[id_last_day_months[j - 1] + 1:last_day])

    return (monthly_data)

def daily_to_annual(daily_data, function):
    nb_years = round(len(daily_data) / 365)
    annual_data = np.zeros(nb_years)

    import numpy.ma.core as ma

    for id_year,year in enumerate(range (0,nb_years,1)):
        if not isinstance(daily_data[id_year*365], ma.MaskedConstant):
            if function == "sum":
                annual_data[id_year] = np.nansum(daily_data[id_year*365:id_year*365+365])
            elif function == "mean":
                annual_data[id_year] = np.nanmean(daily_data[id_year*365:id_year*365+364])
        else :
            print(f"remove year {year}: {daily_data[id_year*365:id_year*365+365]}")

    return annual_data

def monthly_to_annual(monthly_values, function):

    # Convert input to NumPy array
    monthly_values = np.array(monthly_values)

    # Reshape array to have 12 months in each row
    monthly_values_reshaped = monthly_values.reshape(-1, 12)

    # Aggregate monthly values into annual values
    if function == 'sum':
        annual_values = np.sum(monthly_values_reshaped, axis=1)
    elif function == 'average':
        annual_values = np.mean(monthly_values_reshaped, axis=1)
    else:
        raise ValueError("Invalid method. Please choose either 'sum' or 'average'.")

    return annual_values


def convert_day_into_month (day):
    return (np.where(day >= CUMDAY)[0][-1])

def burningdates_to_burningmonths(burningdates):

    months_array = np.zeros(len(burningdates) * 12)
    for i,burningdate in enumerate(burningdates):
        if burningdate > 0 :
            burningmonth = convert_day_into_month(burningdate)
            months_array[burningmonth+(i*12)] = 1
    return (months_array)

def convert_annual_fire_into_monthly(fire_dict,burningmonth_dict,run_config,plot_config):

    burningmonths = burningmonth_dict['output_array']

    annual_fire = import_annual_data(fire_dict,run_config,plot_config)['output_array']
    monthly_fire = np.zeros_like(burningmonths)
    year = 0

    for j, burningmonth in enumerate(burningmonths):
        year = j//12
        if burningmonth > 0:  # indeed if burning months
            monthly_fire[j] = annual_fire[year]

    return (monthly_fire)

def gwp_calculation(gwp_collection, run_config, plot_config):

    # a. Co2 from fire
    # get the data [gCo2/m2/month]
    co2_from_fire_dict = import_monthly_data(gwp_collection['co2_from_fire'], run_config, plot_config)
    # conversion in kgCo2/m2/month
    co2_from_fire_dict['converted_output_array'] = co2_from_fire_dict['output_array'] * 1e-3
    gwp_collection['co2_from_fire'] = co2_from_fire_dict
    # sum up to gwp
    gwp_append(co2_from_fire_dict['converted_output_array'], gwp_collection, plot_config)

    # b. CH4 from fire
    # get the data [gCH4/m2/month]
    ch4_from_fire_dict = import_monthly_data(gwp_collection['ch4_from_fire'], run_config, plot_config)
    # conversion in kgCo2/m2/month
    ch4_from_fire_dict['converted_output_array'] = ch4_from_fire_dict['output_array'] * 1e-3 * 25
    gwp_collection['ch4_from_fire'] = ch4_from_fire_dict
    # sum up to gwp
    gwp_append(ch4_from_fire_dict['converted_output_array'], gwp_collection, plot_config)

    # c. C emissions from soil
    # get the data - take one step ahead in order to calculate emission [gC/m2/day]
    extraction_days = [(plot_config['start_year'] - run_config['start_year']) * 365 - 1,
                       (plot_config['end_year'] - run_config['start_year']) * 365]
    c_from_soil_dict = import_daily_data(gwp_collection['c_from_soil'], run_config, plot_config, extraction_days)
    # calculation emission - difference [gC/m2/day]
    c_from_soil_dict['c_emission'] = c_from_soil_dict['output_array'][1:] - c_from_soil_dict['output_array'][:-1]
    c_from_soil_dict['c_emission'][c_from_soil_dict['c_emission'] < 0] = 0
    # conversion in gC/m2/month sum
    c_from_soil_dict['c_emission'] = daily_to_monthly(c_from_soil_dict['c_emission'], "sum")

    # conversion in kgCo2e/m2/month
    c_from_soil_dict['c_emission'] = c_from_soil_dict['c_emission'] * 1e-3 * (12 + 16 * 2) / 12
    gwp_collection['c_emission'] = c_from_soil_dict
    # sum up to gwp
    gwp_append(c_from_soil_dict['c_emission'], gwp_collection, plot_config)

    # d. N emissions from soil
    # get the data [gN/m2/month]
    n_from_soil_dict = import_monthly_data(gwp_collection['n_from_soil'], run_config, plot_config)
    # conversion in kgCo2/m2/month
    n_from_soil_dict['converted_output_array'] = n_from_soil_dict['output_array'] * 298 * 44 / 14 * 1e-3
    gwp_collection['n_from_soil'] = n_from_soil_dict
    # sum up to gwp
    gwp_append(n_from_soil_dict['converted_output_array'], gwp_collection, plot_config)

    return gwp_collection


def gwp_append(data_array, gwp_collection, plot_config):
    if len(data_array) != plot_config['length_period_month']:
        print("ERROR: the annual {} dont have the expected dimension {}"
              .format(data_array, plot_config['length_period_year']))
        sys.exit()
    if 'annual_gwp' in gwp_collection.keys():
        gwp_collection['annual_gwp'] = gwp_collection['annual_gwp'] + data_array
    else:
        gwp_collection['annual_gwp'] = data_array

    return


def cnratio_calculation(cn_ratio_collection, run_config, plot_config):
    # import data
    daily_leave_c = import_daily_data(cn_ratio_collection['leave_c'], run_config, plot_config)
    daily_leave_n = import_daily_data(cn_ratio_collection['leave_n'], run_config, plot_config)

    # convert daily to monthly
    monthly_leave_c = daily_to_monthly(daily_leave_c['output_array'], 'mean')
    monthly_leave_n = daily_to_monthly(daily_leave_n['output_array'], 'mean')

    return monthly_leave_c / monthly_leave_n

def som_collection_preprocessing (output_collection,run_config,plot_config):

    som_collection = dict()

    # 1. litterfall n
    output_dict = output_collection['daily litterfall n']
    output_dict = import_daily_data(output_dict,run_config,plot_config)
    # conversion daily to monthly
    litterfalln_dict = dict()
    litterfalln_dict['output_array'] = daily_to_monthly(output_dict['output_array'],'sum')
    litterfalln_dict['color'] = output_dict['color']
    litterfalln_dict['label'] = 'litterfall n'

    som_collection['litterfall'] = litterfalln_dict

    # 2. manure
    output_dict = output_collection['daily harvest n']
    output_dict = import_daily_data(output_dict,run_config,plot_config)
    # conversion harvest to manure (manure = 25%, harvestn = 75%)
    manure_dict = dict()
    manure_dict['output_array'] = output_dict['output_array'] * 25 / 75
    manure_dict['output_array'] = daily_to_monthly(manure_dict['output_array'],'sum')
    manure_dict['color'] = output_collection['manure']['color']
    manure_dict['label'] = 'manure'

    som_collection['manure'] = manure_dict

    # 3. immobilisation
    output_dict = output_collection['monthly immobilisation']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    som_collection['immobilisation'] = output_dict
    som_collection['immobilisation']['label'] = 'immobilisation'

    # 4. mineralisation
    output_dict = output_collection['monthly mineralisation']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    som_collection['mineralisation'] = output_dict
    som_collection['mineralisation']['label'] = 'mineralisation'

    # 5. uptake
    output_dict = output_collection['monthly uptake']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    som_collection['uptake'] = output_dict
    som_collection['uptake']['label'] = 'uptake'

    return(som_collection)

def smm_collection_preprocessing (output_collection,run_config,plot_config):

    smm_collection = dict()

    # 1. mineralisation
    output_dict = output_collection['monthly mineralisation']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['mineralisation'] = output_dict
    smm_collection['mineralisation']['label'] = 'mineralisation'

    # 2. bnf
    output_dict = output_collection['monthly bnf']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['bnf'] = output_dict
    smm_collection['bnf']['label'] = 'bnf'

    # 3. deposition
    output_dict = output_collection['daily deposition']
    output_dict['output_array'] = extracting_daily_output_from_listing('deposition',output_dict['file_path'],plot_config)

    deposition_dict = dict()
    deposition_dict['output_array'] = daily_to_monthly(output_dict['output_array'], 'sum')
    deposition_dict['color'] = output_collection['daily deposition']['color']
    deposition_dict['label'] = 'deposition'

    smm_collection['deposition'] = deposition_dict

    # 4. fire n
    output_dict = output_collection['annual firen']
    output_dict['output_array'] = convert_annual_fire_into_monthly(output_dict,output_collection['monthly burning'],run_config,plot_config)
    # ash convertion from firen emission
    output_dict['output_array'] = output_dict['output_array'] * 0.45

    smm_collection['firen'] = output_dict
    smm_collection['firen']['label'] = 'firen'

    # 5. immobilisation
    output_dict = output_collection['monthly immobilisation']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['immobilisation'] = output_dict
    smm_collection['immobilisation']['label'] = 'immobilisation'

    # 6. uptake
    output_dict = output_collection['monthly uptake']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['uptake'] = output_dict
    smm_collection['uptake']['label'] = 'uptake'

    # 7. denitrification
    output_dict = output_collection['monthly denitrification']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['denitrification'] = output_dict
    smm_collection['denitrification']['label'] = 'denitrification'

    # 8. leaching
    output_dict = output_collection['monthly leaching']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['leaching'] = output_dict
    smm_collection['leaching']['label'] = 'leaching'

    # 9. volatisation
    output_dict = output_collection['monthly volatisation']
    output_dict = import_monthly_data(output_dict, run_config, plot_config)

    smm_collection['volatisation'] = output_dict
    smm_collection['volatisation']['label'] = 'volatisation'

    return (smm_collection)


# d) ploting and print functions______________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________
# ____________________________________________________________________________________________________________________________

def print_dict_items(dictionary):
    for key, value in dictionary.items():
        print(f"{key}:{value}")
    return

def print_dict_keys(dictionary):
    for key, value in dictionary.items():
        print(f"{key}")
    return

def create_subplots(n, m, plot_width, plot_height, spacing_horizontal, spacing_vertical):
    """
    Generate a list of subplot coordinates in the format [x_position, y_position, width, height].

    Parameters:
    n (int): Number of horizontal plots.
    m (int): Number of vertical plots.
    plot_width (float): Width of each plot.
    plot_height (float): Height of each plot.
    spacing_horizontal (float): Space between plots horizontally.
    spacing_vertical (float): Space between plots vertically.

    Returns:
    list: List of subplot coordinates.
    """
    subplots = []

    for j in range(m - 1, -1, -1):
        for i in range(n):
            x = i * (plot_width + spacing_horizontal)
            y = j * (plot_height + spacing_vertical)
            subplots.append([x, y, plot_width, plot_height])

    return subplots


def plot_bar_daily(plot_config, subplot, output_dict):
    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]
    xi = np.arange(0, plot_config['length_period_day'], 1)

    axe = plot_config['fig'].add_axes(subplot)
    axe.bar(xi, output_dict['output_array'], color=output_dict['color'])

    axe.set_xticks(np.arange(0, plot_config['length_period_day'], 61))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])
    axe.set_ylabel(output_dict['y_axis_label'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return (axe)


def plot_bar_monthly(plot_config, subplot, output_dict):

    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]

    xi = np.arange(0, plot_config['length_period_month'], 1)

    axe = plot_config['fig'].add_axes(subplot)
    axe.bar(xi, output_dict['output_array'], color=output_dict['color'], align='center', width=1)

    axe.set_xticks(np.arange(0, plot_config['length_period_month'], 2))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])
    axe.set_ylabel(output_dict['y_axis_label'],size=plot_config['axis_label_font_size'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return (axe)

def plot_bar_gwp(plot_config, subplot, output_collection):

    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]
    legends = []

    axe = plot_config['fig'].add_axes(subplot)
    xi = np.arange(0, plot_config['length_period_month'], 1)
    gwp_dict = output_collection['gwp']

    # 1) c_from_soil
    output_dict = output_collection['c_from_soil']
    monthly_gwp = gwp_dict['output_array']
    axe.bar(xi, monthly_gwp, color=output_dict['color'], align='center', width=1)
    subtract_gwp = monthly_gwp - output_dict['c_emission']
    legends.append(mtplines.Line2D([0], [0], color = output_dict['color'],linewidth=7, label=output_dict['label']))

    # 2) n_from_soil
    output_dict = output_collection['n_from_soil']
    axe.bar(xi, subtract_gwp, color=output_dict['color'], align='center', width=1)
    subtract_gwp = subtract_gwp - output_dict['converted_output_array']
    legends.append(mtplines.Line2D([0], [0], color = output_dict['color'],linewidth=7, label=output_dict['label']))

    # 3) ch4_from_fire
    output_dict = output_collection['ch4_from_fire']
    axe.bar(xi, subtract_gwp, color=output_dict['color'], align='center', width=1)
    subtract_gwp = subtract_gwp - output_dict['converted_output_array']
    legends.append(mtplines.Line2D([0], [0], color = output_dict['color'],linewidth=7, label=output_dict['label']))

    # 4) co2_from_fire
    output_dict = output_collection['co2_from_fire']
    axe.bar(xi, subtract_gwp, color=output_dict['color'], align='center', width=1)
    legends.append(mtplines.Line2D([0], [0], color = output_dict['color'],linewidth=7, label=output_dict['label']))

    axe.set_xticks(np.arange(0, plot_config['length_period_month'], 2))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])

    axe.set_ylabel(gwp_dict['y_axis_label'],size=plot_config['axis_label_font_size'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    # legend
    axe.legend(handles = legends,fontsize=plot_config['legend_font_size'],loc = "best",ncol=5)

    return (axe)


def plot_bar_annual(plot_config, subplot, output_dict):
    xi = np.arange(0, plot_config['length_period_year'], 1)

    axe = plot_config['fig'].add_axes(subplot)
    axe.bar(xi, output_dict['output_array'], color=output_dict['color'], align='edge', width=1)

    axe.set_xticks(np.arange(0, plot_config['length_period_year'], 1))
    axe.set_xticklabels(np.arange(plot_config['start_year'], plot_config['end_year'] + 1, 1))
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return (axe)


def plot_daily(plot_config, subplot, output_dict):
    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]

    axe = plot_config['fig'].add_axes(subplot)
    axe.plot(output_dict['output_array'], color=output_dict['color'])

    axe.set_xticks(np.arange(0, plot_config['length_period_day'], 61))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])
    axe.set_ylabel(output_dict['y_axis_label'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return (axe)


def plot_monthly(plot_config, subplot, output_dict):
    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]

    axe = plot_config['fig'].add_axes(subplot)
    axe.plot(output_dict['output_array'], color=output_dict['color'])

    axe.set_xticks(np.arange(0, plot_config['length_period_month'], 2))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])
    axe.set_ylabel(output_dict['y_axis_label'],size=plot_config['axis_label_font_size'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return (axe)

def plot_som_budget(plot_config, subplot, som_collection):

    axe = plot_config['fig'].add_axes(subplot)
    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]

    # 1. litterfall ++
    litterfall_dict = som_collection['litterfall']
    axe.plot(litterfall_dict['output_array'], color=litterfall_dict['color'], linewidth=2)
    xi = np.arange(0, len(litterfall_dict['output_array']), 1)
    axe.fill_between(xi, y1=0, y2=litterfall_dict['output_array'], color=litterfall_dict['color'])

    # 2. manure ++
    manure_dict = som_collection['manure']
    sum_manure = np.add(manure_dict['output_array'], litterfall_dict['output_array'])
    axe.plot(sum_manure, color=manure_dict['color'], linewidth=2)
    axe.fill_between(xi, y1=litterfall_dict['output_array'], y2=sum_manure, color=manure_dict['color'])

    # 3. immobilisation ++
    immobilisation_dict = som_collection['immobilisation']
    sum_immobilisation = np.add(sum_manure,immobilisation_dict['output_array'])
    axe.plot(sum_immobilisation, color=immobilisation_dict['color'], linewidth=2)
    axe.fill_between(xi, y1=sum_manure, y2=sum_immobilisation, color=immobilisation_dict['color'])

    # 4. mineralisation --
    mineralisation_dict = som_collection['mineralisation']
    axe.plot(mineralisation_dict['output_array'] * -1, color=mineralisation_dict['color'], linewidth=2)
    axe.fill_between(xi, y1=0, y2=mineralisation_dict['output_array'] * -1, color=mineralisation_dict['color'])

    # 5. overall budget
    budget = litterfall_dict['output_array'] + manure_dict['output_array'] + immobilisation_dict['output_array'] - mineralisation_dict['output_array']
    axe.plot(budget, color="#000000", linewidth=2, linestyle="dashed")

    axe.set_xticks(np.arange(0, plot_config['length_period_month'], 2))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])

    axe.set_ylabel('SOM-N budget (gN/m2',size=plot_config['axis_label_font_size'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return axe

def plot_smm_budget(plot_config, subplot, smm_collection):

    axe = plot_config['fig'].add_axes(subplot)
    syear = str(plot_config['start_year'])[2:]
    eyear = str(plot_config['end_year'] - 1)[2:]

    # 1. mineralisation ++
    mineralisation_dict = smm_collection['mineralisation']
    axe.plot(mineralisation_dict['output_array'], color=mineralisation_dict['color'], linewidth=0.5)
    xi = np.arange(0, len(mineralisation_dict['output_array']), 1)
    axe.fill_between(xi, y1=0, y2=mineralisation_dict['output_array'], color=mineralisation_dict['color'])

    # 2. bnf ++
    bnf_dict = smm_collection['bnf']
    sum_bnf = np.add(mineralisation_dict['output_array'], bnf_dict['output_array'])
    axe.plot(sum_bnf, color=bnf_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=mineralisation_dict['output_array'], y2=sum_bnf, color=bnf_dict['color'])

    # 3. deposition ++
    deposition_dict = smm_collection['deposition']
    sum_deposition = np.add(sum_bnf,deposition_dict['output_array'])
    axe.plot(sum_deposition, color=deposition_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=sum_bnf, y2=sum_deposition, color=deposition_dict['color'])

    # 4. fire n ++
    firen_dict = smm_collection['firen']
    sum_firen = np.add(sum_deposition, firen_dict['output_array'])
    axe.fill_between(xi, y1=sum_deposition, y2=sum_firen, color=firen_dict['color'])

    # 5. immobilisation --
    immobilisation_dict = smm_collection['immobilisation']
    axe.plot(immobilisation_dict['output_array'] * -1, color=immobilisation_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=0, y2=immobilisation_dict['output_array'] * -1, color=immobilisation_dict['color'])

    # 6. uptake --
    uptake_dict = smm_collection['uptake']
    sum_uptake = np.add(immobilisation_dict['output_array'], uptake_dict['output_array'])
    axe.plot(sum_uptake * -1, color=uptake_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=immobilisation_dict['output_array'] * -1, y2=sum_uptake * -1, color=uptake_dict['color'])

    # 7. denitrification --
    denitrification_dict = smm_collection['denitrification']
    sum_denitrification = np.add(sum_uptake, denitrification_dict['output_array'])
    axe.plot(sum_denitrification * -1, color=denitrification_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=sum_uptake * -1, y2=sum_denitrification * -1, color=denitrification_dict['color'])

    # 8. leaching --
    leaching_dict = smm_collection['leaching']
    sum_leaching = np.add(sum_denitrification, leaching_dict['output_array'])
    axe.plot(sum_leaching * -1, color=leaching_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=sum_denitrification * -1, y2=sum_leaching * -1, color=leaching_dict['color'])

    # 9. volatisation --
    volatisation_dict = smm_collection['volatisation']
    sum_volatisation = np.add(sum_leaching, volatisation_dict['output_array'])
    axe.plot(sum_volatisation * -1, color=volatisation_dict['color'], linewidth=0.5)
    axe.fill_between(xi, y1=sum_leaching * -1, y2=sum_volatisation * -1, color=volatisation_dict['color'])

    # 5. overall budget
    smm_collection['budget'] = (mineralisation_dict['output_array'] + bnf_dict['output_array'] + deposition_dict['output_array'] + firen_dict['output_array']
              - immobilisation_dict['output_array'] - uptake_dict['output_array'] - denitrification_dict['output_array'] - leaching_dict['output_array'] - volatisation_dict['output_array'])
    axe.plot(smm_collection['budget'], color="#000000", linewidth=2, linestyle="dashed")

    axe.set_xticks(np.arange(0, plot_config['length_period_month'], 2))
    axe.set_xticklabels([f"Jan{syear}", f"Mar{syear}", f"May{syear}", f"Jul{syear}", f"Sep{syear}", f"Nov{syear}",
                         f"Jan{eyear}", f"Mar{eyear}", f"May{eyear}", f"Jul{eyear}", f"Sep{eyear}", f"Nov{eyear}"])

    axe.set_ylabel('SMM-N budget (gN/m2',size=plot_config['axis_label_font_size'])
    axe.tick_params(labelsize=10, length=4, direction="inout")

    return axe

def plot_budget_legend(plot_config, subplot, som_collection, smm_collection,output_collection):

    legends = []
    legends.append(mtplines.Line2D([0], [0], color = som_collection['litterfall']['color'],linewidth=7, label=som_collection['litterfall']['label']))
    legends.append(mtplines.Line2D([0], [0], color=som_collection['manure']['color'], linewidth=7, label=som_collection['manure']['label']))
    legends.append(mtplines.Line2D([0], [0], color=som_collection['immobilisation']['color'], linewidth=7, label=som_collection['immobilisation']['label']))
    legends.append(mtplines.Line2D([0], [0], color=som_collection['mineralisation']['color'], linewidth=7, label=som_collection['mineralisation']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['bnf']['color'], linewidth=7, label=smm_collection['bnf']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['deposition']['color'], linewidth=7, label=smm_collection['deposition']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['firen']['color'], linewidth=7, label=smm_collection['firen']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['uptake']['color'], linewidth=7, label=smm_collection['uptake']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['denitrification']['color'], linewidth=7, label=smm_collection['denitrification']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['leaching']['color'], linewidth=7, label=smm_collection['leaching']['label']))
    legends.append(mtplines.Line2D([0], [0], color=smm_collection['volatisation']['color'], linewidth=7, label=smm_collection['volatisation']['label']))
    legends.append(mtplines.Line2D([0], [0], color=output_collection['monthly burning']['color'], linewidth=2, linestyle= 'dashed', label=output_collection['monthly burning']['label']))

    axe = plot_config['fig'].add_axes(subplot,frame_on=False, yticks=[],xticks=[])
    axe.legend(handles = legends,fontsize=plot_config['legend_font_size'],loc = "upper center",ncol=5)

    return (axe)

def plot_fire_month (axe,burning_months_dict):

    burning_months = burning_months_dict['output_array']

    for id_month,months in enumerate(burning_months):
        if months == 1 :
            axe.axvline(x=id_month, ymin=burning_months_dict['minimum'], ymax=burning_months_dict['maximum'], color= burning_months_dict['color'], linewidth=2, linestyle='dashed')

    return

# e) plotting time series ______________________________________________________________________________________________
# POOL _________________________________________________________________________________________________________________
# CARBON _______________________________________________________________________________________________________________
def plot_leavec (run_config,plot_config):

    nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
    daily_leavec = nc.variables['LeafC'][:,0,0] #gC/m2/days
    nc.close()
    leavec = daily_to_annual(daily_leavec,"mean")

    plt.figure(figsize=(20, 3))
    plt.plot(leavec,color="#000000",label="long term")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Leaf C (gC/m2)')

    if run_config['compa'] :
        run2_config = run_config['run2_config']
        nc = Dataset("{}/d_cleaf.nc".format(run2_config['folder_path']), mode='r')
        daily_leavec = nc.variables['LeafC'][:,0,0] #gC/m2/days
        nc.close()
        leavec2 = daily_to_annual(daily_leavec,"mean")
        plt.plot(leavec2,color="#000000",linestyle="dashed",label="short term")
        plt.legend()

    plt.grid(True)
    plt.savefig(f"figures_output/time_series/Leave_C_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_litter_c (run_config,plot_config):

    nc = Dataset("{}/litc.nc".format(run_config['folder_path']), mode='r')
    litc = nc.variables['LitC'][:,0,0] #gC/m2/years
    nc.close()
    average = np.average(litc)
    stdev = np.std(litc)

    plt.figure(figsize=(20, 3))
    plt.plot(litc,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Lit C (gC/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/Lit_C_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_soil_c (run_config,plot_config):

    nc = Dataset("{}/soilc.nc".format(run_config['folder_path']), mode='r')
    soilc = nc.variables['SoilC'][:,0,0] #gC/m2/years
    nc.close()
    average = np.average(soilc)
    stdev =  np.std(soilc)

    plt.figure(figsize=(20, 3))
    plt.plot(soilc,color="#000000",label="burning")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Soil C (gC/m2)')
    plt.grid(True)

    if run_config['compa'] :
        run2_config = run_config['run2_config']
        nc = Dataset("{}/soilc.nc".format(run2_config['folder_path']), mode='r')
        soilc = nc.variables['SoilC'][:,0,0] #gC/m2/years
        nc.close()
        plt.plot(soilc,color="#000000",linestyle="dashed",label="no burning")
        plt.legend()

    plt.savefig(f"figures_output/time_series/Soil_C_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_npp (run_config,plot_config):

    nc = Dataset("{}/d_npp.nc".format(run_config['folder_path']), mode='r')
    npp = nc.variables['NPP'][:,0,0] #gC/m2/days
    nc.close()

    npp = daily_to_annual(npp, "mean") #gC/m2/yrs

    plt.figure(figsize=(20, 3))
    plt.plot(npp,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'NPP (gC/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/NPP_{plot_config['pdf_file']}.png", bbox_inches='tight')

# NITROGEN _____________________________________________________________________________________________________________
def plot_leaven (run_config,plot_config):

    nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
    daily_leaven = nc.variables['nleaf'][:,0,0] #gN/m2/days
    nc.close()
    leaven = daily_to_annual(daily_leaven,"mean")
    average = np.average(leaven)
    stdev =  np.std(leaven)

    # Plotting
    plt.figure(figsize=(20, 3))
    plt.plot(leaven,color="#000000",label="no burning practices")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Leaf N (gN/m2)')

    if run_config['compa'] :
        run2_config = run_config['run2_config']
        nc = Dataset("{}/d_nleaf.nc".format(run2_config['folder_path']), mode='r')
        daily_leaven = nc.variables['nleaf'][:,0,0] #gC/m2/days
        nc.close()
        leaven2 = daily_to_annual(daily_leaven,"mean")
        plt.plot(leaven2,color="#000000",linestyle="dashed",label="no burning practices")
        plt.legend()

    plt.grid(True)
    plt.savefig(f"figures_output/time_series/Leave_N_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_litter_n (run_config,plot_config):

    nc = Dataset("{}/litn.nc".format(run_config['folder_path']), mode='r')
    litn = nc.variables['LitN'][:,0,0] #gN/m2/years
    nc.close()
    average = np.average(litn)
    stdev =  np.std(litn)

    plt.figure(figsize=(20, 3))
    plt.plot(litn,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Lit N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/Lit_N_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_soil_n (run_config,plot_config):

    nc = Dataset("{}/soiln.nc".format(run_config['folder_path']), mode='r')
    soiln = nc.variables['SoilN'][:,0,0] #gN/m2/years
    nc.close()
    average = np.average(soiln)
    stdev =  np.std(soiln)

    plt.figure(figsize=(20, 3))
    plt.plot(soiln,color="#000000",label="burning")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Soil N (gN/m2)')
    plt.grid(True)

    if run_config['compa'] :
        run2_config = run_config['run2_config']
        nc = Dataset("{}/soiln.nc".format(run2_config['folder_path']), mode='r')
        soiln = nc.variables['SoilN'][:,0,0] #gC/m2/years
        nc.close()
        plt.plot(soiln,color="#000000",linestyle="dashed",label="no burning")
        plt.legend()

    plt.savefig(f"figures_output/time_series/Soil_N_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_smm_n (run_config,plot_config):

    nc = Dataset("{}/soilnh4.nc".format(run_config['folder_path']), mode='r')
    soilnh4 = nc.variables['SoilNH4'][:,0,0] #gN/m2/yr
    nc.close()
    nc = Dataset("{}/soilno3.nc".format(run_config['folder_path']), mode='r')
    soilno3 = nc.variables['SoilNO3'][:,0,0] #gN/m2/yr
    nc.close()

    smm = np.add(soilnh4,soilno3)

    plt.figure(figsize=(20, 3))
    plt.plot(smm,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'SMM N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/smm_n_{plot_config['pdf_file']}.png", bbox_inches='tight')

# CN RATIO _____________________________________________________________________________________________________________
def plot_leave_cnratio(run_config,plot_config):

    nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
    daily_leavec = nc.variables['LeafC'][:,0,0] #gC/m2/days
    nc.close()

    nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
    daily_leaven = nc.variables['nleaf'][:,0,0] #gN/m2/days
    nc.close()

    cn_ratio = np.divide(daily_leavec,daily_leaven)
    cn_ratio = daily_to_annual(cn_ratio,"mean")

    # Plotting
    plt.figure(figsize=(20, 3))
    plt.plot(cn_ratio,color="#000000",label="no burning practices")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Leaf CN ratio ()')

    if run_config['compa'] :
        run2_config = run_config['run2_config']
        nc = Dataset("{}/d_cleaf.nc".format(run2_config['folder_path']), mode='r')
        daily_leavec2 = nc.variables['LeafC'][:,0,0] #gC/m2/days
        nc.close()

        nc = Dataset("{}/d_nleaf.nc".format(run2_config['folder_path']), mode='r')
        daily_leaven2 = nc.variables['nleaf'][:,0,0] #gN/m2/days
        nc.close()

        cn_ratio2 = np.divide(daily_leavec2,daily_leaven2)
        cn_ratio2 = daily_to_annual(cn_ratio2,"mean")
        plt.plot(cn_ratio2,color="#000000",linestyle="dashed",label="no burning practices")
        plt.legend()

    plt.grid(True)
    plt.savefig(f"figures_output/time_series/leave_CN_ratio_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_lit_cnratio(run_config,plot_config):

    nc = Dataset("{}/litc.nc".format(run_config['folder_path']), mode='r')
    litc = nc.variables['LitC'][:,0,0] #gC/m2/yrs
    nc.close()

    nc = Dataset("{}/litn.nc".format(run_config['folder_path']), mode='r')
    litn = nc.variables['LitN'][:,0,0] #gN/m2/yrs
    nc.close()

    cn_ratio = np.divide(litc,litn)

    # Plotting
    plt.figure(figsize=(20, 3))
    plt.plot(cn_ratio,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Lit CN ratio ()')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/lit_CN_ratio_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_soil_cnratio(run_config,plot_config):

    nc = Dataset("{}/soilc.nc".format(run_config['folder_path']), mode='r')
    soilc = nc.variables['SoilC'][:,0,0] #gC/m2/yrs
    nc.close()

    nc = Dataset("{}/soiln.nc".format(run_config['folder_path']), mode='r')
    soiln = nc.variables['SoilN'][:,0,0] #gN/m2/yrs
    nc.close()

    cn_ratio = np.divide(soilc,soiln)

    plt.figure(figsize=(20, 3))
    plt.plot(cn_ratio,color="#000000",label="burning")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Soil CN ratio ()')
    plt.grid(True)
    if run_config['compa'] :
        run2_config = run_config['run2_config']

        nc = Dataset("{}/soilc.nc".format(run2_config['folder_path']), mode='r')
        soilc = nc.variables['SoilC'][:,0,0] #gC/m2/yrs
        nc.close()

        nc = Dataset("{}/soiln.nc".format(run2_config['folder_path']), mode='r')
        soiln = nc.variables['SoilN'][:,0,0] #gN/m2/yrs
        nc.close()
        cnratio = soilc/soiln
        plt.plot(cnratio,color="#000000",linestyle="dashed",label="no burning")
        plt.legend()

    plt.savefig(f"figures_output/time_series/soil_CN_ratio_{plot_config['pdf_file']}.png", bbox_inches='tight')

# FLUX _________________________________________________________________________________________________________________
# CARBON

def plot_harvestc (run_config,plot_config):

    nc = Dataset("{}/d_mgrass_harvestc.nc".format(run_config['folder_path']), mode='r')
    harvestc = nc.variables['d_mgrass_harvestc'][:,0,0] #gC/m2/days
    nc.close()
    harvestc = harvestc/3*4 #harvest output only count 75% of total harvest
    harvestc = daily_to_annual(harvestc, "sum") #gC/m2/yrs

    # collapsing ?
    #harvest requirement depends on livestock density
    limit = 4000*365*1e-4*int(run_config['livestock_density'])/10  #gC/m2/yr
    # 0.04 gC/m2/days
    limit80 = limit*80/100 #80% requirement
    collapse = False
    if np.average(harvestc) < limit80 :
        collapse = True

    plt.figure(figsize=(20, 3))
    plt.plot(harvestc,color="#000000",label="short term")
    if collapse == True:
        plt.title(f"{plot_config['plot_title']} - collapse harvestc average = {round(np.average(harvestc),2)} - limit 80={round(limit80,2)}")
    else :
        plt.title(f"{plot_config['plot_title']} - harvestc average = {round(np.average(harvestc),2)} - limit 80={round(limit80,2)}")
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Harvest C (gC/m2)')
    plt.axhline(y=limit80, color='r', linestyle='--')
    plt.grid(True)
    if run_config['compa'] :
        run2_config = run_config['run2_config']

        nc = Dataset("{}/d_mgrass_harvestc.nc".format(run2_config['folder_path']), mode='r')
        harvestc = nc.variables['d_mgrass_harvestc'][:,0,0] #gC/m2/days
        nc.close()

        harvestc = harvestc/3*4 #harvest output only count 75% of total harvest
        harvestc = daily_to_annual(harvestc, "sum") #gC/m2/yrs

        plt.plot(harvestc,color="#000000",linestyle="dashed",label="long term")
        plt.legend()
    plt.savefig(f"figures_output/time_series/harvestc_{plot_config['pdf_file']}.png", bbox_inches='tight')


# NITROGEN _____________________________________________________________________________________________________________
def plot_manure (run_config,plot_config):

    nc = Dataset("{}/d_mgrass_harvestn.nc".format(run_config['folder_path']), mode='r')
    harvestn = nc.variables['d_mgrass_harvestn'][:,0,0] #gN/m2/days
    nc.close()
    daily_manure = harvestn * 25/75
    #manure = daily_to_annual(manure, "sum") #gN/m2/yrs
    manure = np.zeros(run_config['nb_of_year'])
    for id_year in range(0,run_config['nb_of_year']):
        manure[id_year] = daily_manure[id_year*365+365]

    plt.figure(figsize=(20, 3))
    plt.plot(manure,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Manure N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/manure_{plot_config['pdf_file']}.png", bbox_inches='tight')
def plot_litterfall_n (run_config,plot_config):

    nc = Dataset("{}/d_mgrass_litfalln.nc".format(run_config['folder_path']), mode='r')
    litterfalln = nc.variables['d_mgrass_litfalln'][:,0,0] #gN/m2/days
    nc.close()

    litterfalln = daily_to_annual(litterfalln, "sum") #gN/m2/yrs

    plt.figure(figsize=(20, 3))
    plt.plot(litterfalln,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Litterfall N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/litterfall_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_immobilisation (run_config,plot_config):

    nc = Dataset("{}/n_immobilization.nc".format(run_config['folder_path']), mode='r')
    immobilisation = nc.variables['N_immobilization'][:,0,0] #gN/m2/months
    nc.close()

    immobilisation = monthly_to_annual(immobilisation, "sum") #gN/m2/years

    plt.figure(figsize=(20, 3))
    plt.plot(immobilisation,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Immobilisation N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/immobilisation_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_mineralisation (run_config,plot_config):

    nc = Dataset("{}/n_mineralization.nc".format(run_config['folder_path']), mode='r')
    mineralization = nc.variables['N_mineralization'][:,0,0] #gN/m2/months
    nc.close()

    mineralization = monthly_to_annual(mineralization, "sum") #gN/m2/years

    plt.figure(figsize=(20, 3))
    plt.plot(mineralization,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Mineralization N (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/mineralization_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_budgetSOM (run_config,plot_config):

    nc = Dataset("{}/d_mgrass_harvestn.nc".format(run_config['folder_path']), mode='r')
    harvestn = nc.variables['d_mgrass_harvestn'][:,0,0] #gN/m2/days
    nc.close()
    daily_manure = harvestn * 25/75
    manure = daily_to_annual(daily_manure, "sum") #gN/m2/yrs

    nc = Dataset("{}/d_mgrass_litfalln.nc".format(run_config['folder_path']), mode='r')
    litterfalln = nc.variables['d_mgrass_litfalln'][:,0,0] #gN/m2/days
    nc.close()

    litterfalln = daily_to_annual(litterfalln, "sum") #gN/m2/yrs

    nc = Dataset("{}/n_immobilization.nc".format(run_config['folder_path']), mode='r')
    immobilisation = nc.variables['N_immobilization'][:,0,0] #gN/m2/months
    nc.close()

    immobilisation = monthly_to_annual(immobilisation, "sum") #gN/m2/years

    som_in = manure + litterfalln +immobilisation

    nc = Dataset("{}/n_mineralization.nc".format(run_config['folder_path']), mode='r')
    mineralization = nc.variables['N_mineralization'][:,0,0] #gN/m2/months
    nc.close()

    mineralization = monthly_to_annual(mineralization, "sum") #gN/m2/years
    som_out = mineralization

    plt.figure(figsize=(20, 3))
    plt.plot(som_in,color="#000000",label="SOM-N in")
    plt.plot(som_out,color="#ff0000",linestyle="dashed",label="SOM-N out")
    plt.ylim(0, 14)
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'SOM-N Budget (gN/m2)')
    plt.grid(True)
    plt.legend()
    plt.savefig(f"figures_output/time_series/som_n_budget_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_budgetSMM (run_config,plot_config):

    nc = Dataset("{}/d_mgrass_harvestn.nc".format(run_config['folder_path']), mode='r')
    harvestn = nc.variables['d_mgrass_harvestn'][:,0,0] #gN/m2/days
    nc.close()
    daily_manure = harvestn * 25/75
    manure = daily_to_annual(daily_manure, "sum") #gN/m2/yrs

    nc = Dataset("{}/d_mgrass_litfalln.nc".format(run_config['folder_path']), mode='r')
    litterfalln = nc.variables['d_mgrass_litfalln'][:,0,0] #gN/m2/days
    nc.close()

    litterfalln = daily_to_annual(litterfalln, "sum") #gN/m2/yrs

    nc = Dataset("{}/n_immobilization.nc".format(run_config['folder_path']), mode='r')
    immobilisation = nc.variables['N_immobilization'][:,0,0] #gN/m2/months
    nc.close()

    immobilisation = monthly_to_annual(immobilisation, "sum") #gN/m2/years

    som_in = manure + litterfalln +immobilisation

    nc = Dataset("{}/n_mineralization.nc".format(run_config['folder_path']), mode='r')
    mineralization = nc.variables['N_mineralization'][:,0,0] #gN/m2/months
    nc.close()

    mineralization = monthly_to_annual(mineralization, "sum") #gN/m2/years
    som_out = mineralization

    plt.figure(figsize=(20, 3))
    plt.plot(som_in,color="#000000",label="SOM-N in")
    plt.plot(som_out,color="#ff0000",linestyle="dashed",label="SOM-N out")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'SOM-N Budget (gN/m2)')
    plt.grid(True)
    plt.legend()
    plt.savefig(f"figures_output/time_series/smm_n_budget_{plot_config['pdf_file']}.png", bbox_inches='tight')


def plot_uptake (run_config,plot_config):

    nc = Dataset("{}/nuptake.nc".format(run_config['folder_path']), mode='r')
    uptake = nc.variables['N_uptake'][:,0,0] #gN/m2/month
    nc.close()

    uptake = monthly_to_annual(uptake, "sum")

    # Plotting
    plt.figure(figsize=(20, 3))
    plt.plot(uptake,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Uptake (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/uptake_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_denitrification (run_config,plot_config):

    nc = Dataset("{}/n2o_denit.nc".format(run_config['folder_path']), mode='r')
    denitrification = nc.variables['N2O_denitrification'][:,0,0] #gN/m2/months
    nc.close()

    denitrification = monthly_to_annual(denitrification, "sum")

    # Plotting
    plt.figure(figsize=(20, 3))
    plt.plot(denitrification,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Denitrification (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/denitrification_{plot_config['pdf_file']}.png", bbox_inches='tight')

# BUDGET _______________________________________________________________________________________________________________

def plot_som_budget_ts (run_config,plot_config):

    nc = Dataset("{}/soiln.nc".format(run_config['folder_path']), mode='r')
    years = nc.variables['time'][:]
    y1 = np.where(years == plot_config['start_year'])[0][0]
    y2 = np.where(years == plot_config['end_year'])[0][0]+1
    som_n_n1 = nc.variables['SoilN'][y1+1:y2,0,0] #gN/m2/yr
    som_n_m1 = nc.variables['SoilN'][y1:y2-1,0,0] #gN/m2/yr
    nc.close()

    som_n_budget = som_n_n1 - som_n_m1

    plt.figure(figsize=(20, 3))
    plt.plot(som_n_budget,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'SOM N Budget (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/som_n_budget_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_smm_budget_ts (run_config,plot_config):

    nc = Dataset("{}/soilnh4.nc".format(run_config['folder_path']), mode='r')
    soilnh4 = nc.variables['SoilNH4'][:,0,0] #gN/m2/yr
    nc.close()
    nc = Dataset("{}/soilno3.nc".format(run_config['folder_path']), mode='r')
    soilno3 = nc.variables['SoilNO3'][:,0,0] #gN/m2/yr
    nc.close()

    smm = np.add(soilnh4,soilno3)
    smm_n1 = smm[1:]
    smm_m1 = smm[:-1]
    smm_budget = smm_n1 - smm_m1

    plt.figure(figsize=(20, 3))
    plt.plot(smm_budget,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'SMM N Budget (gN/m2)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/smm_n_budget_{plot_config['pdf_file']}.png", bbox_inches='tight')

# SOIL FEATURE _________________________________________________________________________________________________________

def plot_soiltemp (run_config,plot_config):

    nc = Dataset("{}/soiltemp.nc".format(run_config['folder_path']), mode='r')
    soiltemp = nc.variables['soiltemp'][:,:,0,0] #°C/months
    nc.close()

    # average over layers
    soiltemp = np.average(soiltemp,axis=1)
    soiltemp = monthly_to_annual(soiltemp, "average") #°C/years

    plt.figure(figsize=(20, 3))
    plt.plot(soiltemp,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Soil temperature (°C)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/soiltemp_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_soil_water_content (run_config,plot_config):

    nc = Dataset("{}/swc.nc".format(run_config['folder_path']), mode='r')
    swc = nc.variables['SWC'][:,:,0,0] #fraction/months
    nc.close()

    # average over layers
    swc = np.average(swc,axis=1)
    swc = monthly_to_annual(swc, "average") #fraction/years

    plt.figure(figsize=(20, 3))
    plt.plot(swc,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Soil Water content (frac)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/soilwc_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_litter_moisture(run_config,plot_config):

    nc = Dataset("{}/combustion_efficiency.nc".format(run_config['folder_path']), mode='r')
    combustion_eff = nc.variables['COMBUSTION_EFFICIENCY'][:,0,0] #fraction/months
    nc.close()

    plt.figure(figsize=(20, 3))
    plt.plot(combustion_eff,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Combustion Eff (frac)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/comb_eff_{plot_config['pdf_file']}.png", bbox_inches='tight')

# CLIMATE CONDITIONS ___________________________________________________________________________________________________

def plot_temperature (lat,lon,plot_config,run_config):

    nc = Dataset("C:/Users/brune\Documents\local_data_cluster\climate_output/temp.nc", mode='r')
    latitudes = nc.variables['latitude'][:]
    longitudes = nc.variables['longitude'][:]
    latitude = np.where(latitudes == lat)[0][0]
    longitude = np.where(longitudes == lon)[0][0]
    temperature = nc.variables['temp'][:,latitude,longitude] #°C/days
    nc.close()

    temperature = daily_to_annual(temperature, "mean") #°C/years

    plt.figure(figsize=(20, 3))
    plt.plot(temperature,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Temperature (°C)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/temperature_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_precipitation(run_config,plot_config):

    nc = Dataset("{}/mprec.nc".format(run_config['folder_path']), mode='r')
    precipitation = nc.variables['prec'][0:12*5,0,0] #mm/months
    nc.close()

    #precipitation = monthly_to_annual(precipitation, "average") #mm/years

    plt.figure(figsize=(20, 3))
    plt.plot(precipitation,color="#000000")
    plt.title(plot_config['plot_title'])
    plt.xlabel('Years')
    plt.ylabel(f'Precipitation (mm)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/precipitation_{plot_config['pdf_file']}.png", bbox_inches='tight')

def plot_seasonality(run_config,plot_config):

    nc = Dataset("{}/seasonality.nc".format(run_config['folder_path']), mode='r')
    seasonality = nc.variables['seasonality'][:,0,0]
    nc.close()
    print("Seasonality: ",seasonality)

def plot_burningdate(run_config,plot_config):

    nc = Dataset("{}/burningdate.nc".format(run_config['folder_path']), mode='r')
    burningdate = nc.variables['BURNINGDATE'][:,0,0] #burning date/yr
    nc.close()
    burningdate1 = []
    for bd in burningdate :
        if bd > 0:
            burningdate1.append(bd)
        else :
            burningdate1.append(burningdate1[-1])

    plt.figure(figsize=(20, 3))
    plt.plot(burningdate1,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'Burning date ()')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/burningdate_{plot_config['pdf_file']}.png", bbox_inches='tight')

# FIRE EMISSIONS _______________________________________________________________________________________________________

def fireemissions_co2e(run_config,plot_config):

    nc = Dataset("{}/fireemission_co2.nc".format(run_config['folder_path']), mode='r')
    co2_emission = nc.variables['co2_emission'][:,0,0] # gCO2/m2/month
    nc.close()

    co2_emission = monthly_to_annual(co2_emission, "sum") #gCO2/m2/year
    co2_emission = co2_emission * 1e-3 #kgCO2/m2/year

    nc = Dataset("{}/fireemission_ch4.nc".format(run_config['folder_path']), mode='r')
    mfirech4 = nc.variables['ch4_emission'][:,0,0] #gCH4/m2/month
    nc.close()

    firech4 = monthly_to_annual(mfirech4, "sum") #gCH4/m2/year
    firech4 = firech4 * 1e-3 #kgCH4/m2/year

    fire_emission = co2_emission + firech4

    plt.figure(figsize=(20, 3))
    plt.plot(fire_emission,color="#000000")
    plt.title(plot_config['plot_title'])
    year_ticks = range(2, run_config['nb_year'], 5)
    plt.xticks(year_ticks, [str(year) for year in range(run_config['start_year']+2, run_config['end_year'],5)])
    plt.xlabel('Years')
    plt.ylabel(f'fire emission (co2e)')
    plt.grid(True)
    plt.savefig(f"figures_output/time_series/fire_emission_co2e_{plot_config['pdf_file']}.png", bbox_inches='tight')

# OLD VERSION

# EMISSION _____________________________________________________________________________________________________________

def plot_co2_from_fire (run_config,plot_config):

    nc = Dataset("{}/fireemission_co2.nc".format(run_config['folder_path']), mode='r')
    co2_emission = nc.variables['co2_emission'][:,0,0] # gCO2/m2/month
    nc.close()

    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(co2_emission)
    plt.title(plot_config['plot_title'])
    plt.xlabel('Years')
    plt.ylabel(f'co2_emission (gCo2/m2)')
    plt.grid(True)
    plt.show()

def plot_ch4_from_fire (run_config,plot_config):

    nc = Dataset("{}/fireemission_ch4.nc".format(run_config['folder_path']), mode='r')
    ch4_emission = nc.variables['ch4_emission'][:,0,0] # gCH4/m2/month
    nc.close()

    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(ch4_emission)
    plt.title(plot_config['plot_title'])
    plt.xlabel('Years')
    plt.ylabel(f'ch4_emission (gCH4/m2)')
    plt.grid(True)
    plt.show()



def plot_recovery_time(year,run_config,plot_config):

    relative_year = year-run_config['start_year']

    # import daily leaf c
    nc = Dataset(f"{run_config['folder_path']}/d_cleaf.nc", mode='r')
    days = nc.variables['time'][:]
    start_index = days[relative_year*365]
    end_index = days[(relative_year+3)*365-1]
    leavec = nc.variables['LeafC'][start_index:end_index,0,0] #gN/m2/days
    nc.close()

    # import burning date
    nc = Dataset(f"{run_config['folder_path']}/burningdate.nc", mode='r')
    years = nc.variables['time'][:]
    year_index = np.where(years == year)[0][0]
    burningdates = nc.variables['BURNINGDATE'][year_index,0,0]
    nc.close()

    plt.figure(figsize=(10, 6))
    plt.plot(leavec,color='black')
    plt.title(plot_config['plot_title'])
    plt.xlabel('Years')
    plt.ylabel(f'leave C (gN/m2)')
    plt.grid(True)
    plt.show()

def set_subplots (n,m,h,l,eh,ev):
    ''' return a list of subplotpython how to [xposition,yposision,width,high]\n
    n: number of horizontal plots; m: number of vertical plots;\n
    h: high of plots; l: larger of plots;\n
    eh: space between plots horizontally; ev: space between plots vertically\n
    '''
    subplots = []
    for j in range (m-1,-1,-1):
        for i in range (0,n,1):
            subplots.append([i*(l+eh),j*(h+ev),l,h])
    return (subplots)

def annual_leaf_cnratio (folder):
    # CN ratio
    # import daily leaf C
    nc = Dataset(f"{folder}/d_cleaf.nc", mode='r')
    daily_leafc = nc.variables['LeafC'][:, 0, 0]  # gC/m2/day
    nc.close()
    annual_leafc = daily_to_annual(daily_leafc, "mean") # gC/m2/year

    # import daily leaf N
    nc = Dataset(f"{folder}/d_nleaf.nc", mode='r')
    daily_leafn = nc.variables['nleaf'][:, 0, 0]  # gN/m2/day
    nc.close()
    annual_leafn = daily_to_annual(daily_leafn, "mean") # gN/m2/year

    return np.divide(annual_leafc, annual_leafn)

def annual_soil_cnratio(folder):
    nc = Dataset(f"{folder}/soilc.nc", mode='r')
    soilc = nc.variables['SoilC'][:,0,0] #gC/m2/yrs
    nc.close()

    nc = Dataset(f"{folder}/soiln.nc", mode='r')
    soiln = nc.variables['SoilN'][:,0,0] #gN/m2/yrs
    nc.close()

    return np.divide(soilc, soiln)

def annual_harvestc(folder):
    # Harvest C
    nc = Dataset(f"{folder}/d_mgrass_harvestc.nc", mode='r')
    daily_harvestc = nc.variables['d_mgrass_harvestc'][:, 0, 0]  # gC/m2/day
    nc.close()
    daily_harvestc = daily_harvestc / 3 * 4  # harvest output only count 75% of total harvest

    return daily_to_annual(daily_harvestc, "sum") # gC/m2/year

def annual_gwp_calculation (folder):
    # gwp - carbon emission
    total_carbon_emission= np.zeros(70)

    # co2 emission from fire
    nc = Dataset(f"{folder}/fireemission_co2.nc", mode='r')
    monthly_co2_from_fire = nc.variables['co2_emission'][:, 0, 0]  # gCO2/m2/month
    nc.close()
    monthly_co2_from_fire = monthly_co2_from_fire * 1e-3 # kgCO2/m2/month
    annual_co2_from_fire = monthly_to_annual(monthly_co2_from_fire, "sum") # kgCO2/m2/year
    total_carbon_emission = total_carbon_emission + annual_co2_from_fire

    # ch4 emission from fire
    nc = Dataset(f"{folder}/fireemission_ch4.nc", mode='r')
    monthly_ch4_from_fire = nc.variables['ch4_emission'][:, 0, 0]  # gCH4/m2/month
    nc.close()
    monthly_ch4_from_fire = monthly_ch4_from_fire * 1e-3 * 25 # kgCo2/m2/month
    annual_ch4_from_fire = monthly_to_annual(monthly_ch4_from_fire, "sum") # kgCO2/m2/year
    total_carbon_emission = total_carbon_emission + annual_ch4_from_fire

    # c from soil
    nc = Dataset(f"{folder}/d_mgrass_soilc.nc", mode='r')
    daily_soil_c = nc.variables['d_mgrass_soilc'][:, 0, 0]  # gC/m2/day
    nc.close()
    daily_c_from_soil = daily_soil_c[1:] - daily_soil_c[:-1]
    daily_c_from_soil[daily_c_from_soil < 0] = 0
    daily_c_from_soil = daily_c_from_soil * 1e-3 * (12 + 16 * 2) / 12 # kgCo2/m2/day
    annual_c_from_soil = daily_to_annual(daily_c_from_soil,"sum") # kgCo2/m2/year
    total_carbon_emission = total_carbon_emission + annual_c_from_soil

    # n from soil
    nc = Dataset(f"{folder}/n2o_denit.nc", mode='r')
    monthly_soil_n_emission = nc.variables['N2O_denitrification'][:, 0, 0]  # gN/m2/month
    nc.close()
    monthly_soil_n_emission = monthly_soil_n_emission * 298 * 44 / 14 * 1e-3 # kgCo2/m2/month
    annual_soil_n_emission = monthly_to_annual(monthly_soil_n_emission,"sum") # kgCo2/m2/year
    total_carbon_emission = total_carbon_emission + annual_soil_n_emission

    return total_carbon_emission

def import_burning_date(folder):

    nc = Dataset(f"{folder}/burningdate.nc", mode='r')
    burning_dates = nc.variables['BURNINGDATE'][:, 0, 0]
    nc.close()
    return burning_dates

def get_size_marker_from_co2_emission (co2_emission):

                                if co2_emission < 0.4:
                                    return 4
                                elif co2_emission < 0.8:
                                    return 8
                                elif co2_emission < 0.12:
                                    return 16
                                elif co2_emission < 0.16:
                                    return 64
                                else :
                                    return 64*2

def frequency_index (freq):
    if freq == 2 :
        return 0
    elif freq == 5:
        return 1
    elif freq == 10:
        return 2

def get_size_marker_from_harvest (harvest_relative):
                            if harvest_relative < 20:
                                return 10
                            elif harvest_relative >= 20:
                                return 50

def get_color_list_from_term (term):
                if term == "short":
                    return color_list_short_term
                elif term == "long":
                    return color_list_long_term

def exclusion_scenario (biome,freq,date,term):
    # exclusion scenario
    if biome == "cerrado" and freq == 2 and date == 1 and term == "short":
        return 1
    elif biome == "cerrado" and freq == 10 and date == 4 and term == "short":
        return 1
    elif biome == "caatinga" and freq == 2 and date == 1 and term == "short":
        return 1
    elif biome == "caatinga" and freq == 2 and date == 2 and term == "short":
        return 1
    elif biome == "mata1" and date == 1 and term == "short":
        return 1
    elif biome == "mata1" and date == 2 and term == "short":
        return 1
    elif biome == "mata1" and freq == 5 and date == 1 and term == "long":
        return 1
    elif biome == "mata1" and freq == 10 and date == 1 and term == "long":
        return 1
    elif biome == "mata1" and freq == 10 and date == 2 and term == "long":
        return 1
    else:
        return 0

def import_soiln_positive_fluxes (run_config,plot_config):

    output_collection = output_dictionnary(run_config)

    # Litterfall n
    output_dict = output_collection['annual litterfall n']
    litterfalln = np.average(import_annual_data(output_dict,run_config,plot_config)['output_array'])

    # Manure
    output_dict = output_collection['annual harvest n']
    manure = np.average(import_annual_pft_data(output_dict,run_config,plot_config)['output_array'])
    manure = manure * 25 / 75

     # Deposition
    output_dict = output_collection['daily deposition']
    output_dict['output_array'] = extracting_daily_output_from_listing('deposition',output_dict['file_path'],plot_config)
    deposition = np.average(daily_to_annual(output_dict['output_array'], 'sum'))

    # Fire n
    output_dict = output_collection['annual firen']
    fire = np.average(import_annual_data(output_dict,run_config,plot_config)['output_array'])
    fire = fire * 0.45

    # BNF
    output_dict = output_collection['monthly bnf']
    bnf = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    bnf = np.average(monthly_to_annual(bnf,'sum'))

    return [litterfalln,manure,deposition,fire,bnf]


def import_soiln_negative_fluxes (run_config,plot_config):

    output_collection = output_dictionnary(run_config)
    # Leaching
    output_dict = output_collection['monthly leaching']
    leaching = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    leaching = np.average(monthly_to_annual(leaching,'sum'))

    # Denitrification
    output_dict = output_collection['monthly denitrification']
    denitrification = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    denitrification = np.average(monthly_to_annual(denitrification,'sum'))

    # Volatisation
    output_dict = output_collection['monthly volatisation']
    volatilisation = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    volatilisation = np.average(monthly_to_annual(volatilisation,'sum'))

    # Uptake
    output_dict = output_collection['monthly uptake']
    uptake = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    uptake = np.average(monthly_to_annual(uptake,'sum'))

    return [-leaching,-denitrification,-volatilisation,-uptake]

def import_nitrogen_positive_fluxes (run_config,plot_config):

    output_collection = output_dictionnary(run_config)

     # Deposition
    output_dict = output_collection['daily deposition']
    output_dict['output_array'] = extracting_daily_output_from_listing('deposition',output_dict['file_path'],plot_config)
    deposition = np.average(daily_to_annual(output_dict['output_array'], 'sum'))

    # BNF
    output_dict = output_collection['monthly bnf']
    bnf = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    bnf = np.average(monthly_to_annual(bnf,'sum'))


    return [deposition+bnf]

def import_nitrogen_negative_fluxes (run_config,plot_config):

    output_collection = output_dictionnary(run_config)

    # air N2o (deni + nitrification) = lpjml denitrification + n2 emission
    output_dict = output_collection['monthly denitrification']
    #denitrification = np.average(import_monthly_data(output_dict, run_config, plot_config)['output_array'])
    denitrification = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    denitrification = np.average(monthly_to_annual(denitrification,'sum'))

    # Leaching
    output_dict = output_collection['monthly leaching']
    leaching = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    leaching = np.average(monthly_to_annual(leaching,'sum'))

    # Volatisation
    output_dict = output_collection['monthly volatisation']
    #volatilisation = np.average(import_monthly_data(output_dict, run_config, plot_config)['output_array'])
    volatilisation = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    volatilisation = np.average(monthly_to_annual(volatilisation,'sum'))

    # harvestn
    output_dict = output_collection['daily harvest n']
    harvestn = import_daily_data(output_dict, run_config, plot_config)['output_array']
    harvestn = np.average(daily_to_annual(harvestn,'sum'))
    # convertion to cry matter intake
    harvestn = 0.4160927 * harvestn

    # fire emission
    output_dict = output_collection['nox emission']
    nox = import_monthly_data(output_dict, run_config, plot_config)['output_array']
    nox = np.average(monthly_to_annual(nox,'sum'))



    return [-leaching,-denitrification,-volatilisation,-harvestn,-nox]
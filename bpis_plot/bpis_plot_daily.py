# 1. imports and global variables
import numpy as np
from netCDF4 import *
import sys
import matplotlib.lines as mtplines
import matplotlib.pyplot as plt
import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "cerrado"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "0" # 0/01/05
run_config['burning_frequency'] = 0 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 1 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017
run_config['nb_of_year'] = run_config['end_year'] - run_config['start_year']
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])
run_config['nb_year'] = run_config['end_year'] - run_config['start_year']

# a) run configuration
run2_config = dict()
run2_config['biome'] = "cerrado"
run2_config['pixel'] = sf.get_biome_pixel (run2_config['biome'])
run2_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run2_config['biome'],run2_config['pixel'])
run2_config['practice_term'] = "long" # short/long
run2_config['livestock_density'] = "01" # 0/01/05
run2_config['burning_frequency'] = 5 # 0/2/5/10
run2_config['burning_phase'] = 0 # [0,burning_frequency,1]
run2_config['burning_date'] = 1 # 1/2/3/4
run2_config['start_year'] = 1948
run2_config['end_year'] = 2017
run2_config['nb_of_year'] = run2_config['end_year'] - run2_config['start_year']
run2_config['scaling'] = False # True/False
if run2_config['burning_frequency'] == 0 :
    run2_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run2_config['run_collection_path'],run2_config['pixel'],run2_config['practice_term'],run2_config['livestock_density'])
else :
    run2_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run2_config['run_collection_path'],run2_config['pixel'],run2_config['practice_term'],run2_config['livestock_density'],
    run2_config['burning_frequency'],run2_config['burning_phase'],run2_config['burning_date'])
run2_config['nb_year'] = run2_config['end_year'] - run2_config['start_year']


# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 1984 # 99 = no el nino
plot_config['end_year'] = 1988
plot_config['nb_years'] = plot_config['end_year'] - plot_config['start_year']
plot_config['start_day'] = (plot_config['start_year'] - run_config['start_year'])*365
plot_config['end_day'] = (plot_config['end_year'] - run_config['start_year'])*365-5
plot_config['nb_days'] = plot_config['end_day'] - plot_config['start_day']
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "plot_overview"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11

syear = str(plot_config['start_year'])[2:]
eyear = str(plot_config['end_year'] - 1)[2:]
xticklabels = []
for year in range(0,plot_config['nb_years'],1):
    xticklabels.append(f"Jan{plot_config['start_year']+year-1900}")
    xticklabels.append(f"Mar{plot_config['start_year']+year-1900}")
    xticklabels.append(f"May{plot_config['start_year']+year-1900}")
    xticklabels.append(f"Jul{plot_config['start_year']+year-1900}")
    xticklabels.append(f"Sep{plot_config['start_year']+year-1900}")
    xticklabels.append(f"Nov{plot_config['start_year']+year-1900}")


# daily NPP
nc = Dataset("{}/d_npp.nc".format(run_config['folder_path']), mode='r')
npp = nc.variables['NPP'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(npp,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily NPP (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_npp_{plot_config['pdf_file']}.png", bbox_inches='tight')

# daily bm inc
def bm_inc_calculation():

    # daily leave C
    nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
    leafc = nc.variables['LeafC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
    nc.close()

    # daily leave N
    nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
    leafn = nc.variables['nleaf'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
    nc.close()

    nc = Dataset("{}/d_npp.nc".format(run_config['folder_path']), mode='r')
    npp = nc.variables['NPP'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
    nc.close()

    # daily leaf CN ratio :
    leaf_cn_ratio = leafc/leafn
    # allocation = when cnratio [t] != cnratio [t-1]
    allocation = leaf_cn_ratio[1:] - leaf_cn_ratio[:-1]
    allocation_leafc = leafc[1:] - leafc[:-1]

    bminc = []
    tmp_bminc = 0

    for id_npp, day_npp in enumerate(npp[1:]):

        tmp_bminc = tmp_bminc + day_npp
        bminc.append(tmp_bminc)
        if abs(allocation[id_npp]) > 0.00001 : #if allocation
            tmp_bminc = 0
        else :
            if abs(allocation_leafc[id_npp]) > 0.001 :
                tmp_bminc = 0


    return (bminc)

bminc = bm_inc_calculation()
plt.figure(figsize=(20, 3))
plt.plot(bminc,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily bminc (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_bminc_{plot_config['pdf_file']}.png", bbox_inches='tight')

# plot npp
nc = Dataset("{}/d_npp.nc".format(run_config['folder_path']), mode='r')
npp = nc.variables['NPP'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(npp,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'NPP (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_npp_{plot_config['pdf_file']}.png", bbox_inches='tight')


'''
# daily harvetc
nc = Dataset("{}/d_mgrass_harvestc.nc".format(run_config['folder_path']), mode='r')
harvestc = nc.variables['d_mgrass_harvestc'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(harvestc,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily harvetC (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_harvetC_{plot_config['pdf_file']}.png", bbox_inches='tight')

# daily litterfall N
nc = Dataset("{}/d_mgrass_litfalln.nc".format(run_config['folder_path']), mode='r')
litterfalln = nc.variables['d_mgrass_litfalln'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(litterfalln,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily litterfallN (gN/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_litterfalln_{plot_config['pdf_file']}.png", bbox_inches='tight')


# daily leave C
nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
leafc = nc.variables['LeafC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

nc = Dataset("{}/d_cleaf.nc".format(run2_config['folder_path']), mode='r')
leafc2 = nc.variables['LeafC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(leafc,color="#000000",label="early season")
#plt.plot(leafc2,color="#000000",linestyle="dashed",label="end season")
#plt.legend()
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily leafC (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_leafc_{plot_config['pdf_file']}.png", bbox_inches='tight')


# daily leave N
nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
leafn = nc.variables['nleaf'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

nc = Dataset("{}/d_nleaf.nc".format(run2_config['folder_path']), mode='r')
leafn2 = nc.variables['nleaf'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(leafn,color="#000000",label="early season")
#plt.plot(leafc2,color="#000000",linestyle="dashed",label="end season")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily leafN (gN/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_leafn_{plot_config['pdf_file']}.png", bbox_inches='tight')

# daily leaf CN ratio :
leaf_cn_ratio = leafc/leafn
plt.figure(figsize=(20, 3))
plt.plot(leaf_cn_ratio,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily CN ratio')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_leaf_cnratio_{plot_config['pdf_file']}.png", bbox_inches='tight')

# daily soil C
nc = Dataset("{}/d_mgrass_soilc.nc".format(run_config['folder_path']), mode='r')
soilc = nc.variables['d_mgrass_soilc'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(soilc,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], 30+31),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily Soil C (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_soilc_{plot_config['pdf_file']}.png", bbox_inches='tight')


# daily soil water content layer 0
nc = Dataset("{}/d_w0.nc".format(run_config['folder_path']), mode='r')
w0 = nc.variables['w0'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(w0,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], (plot_config['nb_days']/(12*plot_config['nb_years']))*2),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily Soil water content layer 0 (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/daily_w0_{plot_config['pdf_file']}.png", bbox_inches='tight')


# daily N root
nc = Dataset("{}/d_nroot.nc".format(run_config['folder_path']), mode='r')
nroot = nc.variables['nroot'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(nroot,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], (plot_config['nb_days']/(12*plot_config['nb_years']))*2),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily N root (gN/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/d_nroot_{plot_config['pdf_file']}.png", bbox_inches='tight')

# daily C root
nc = Dataset("{}/d_croot.nc".format(run_config['folder_path']), mode='r')
croot = nc.variables['RootC'][plot_config['start_day']:plot_config['end_day'],0,0] #gC/m2/days
nc.close()

plt.figure(figsize=(20, 3))
plt.plot(croot,color="#000000")
plt.title(plot_config['plot_title'])
plt.xticks(np.arange(0, plot_config['nb_days'], (plot_config['nb_days']/(12*plot_config['nb_years']))*2),xticklabels)
plt.tick_params(labelsize=10, length=4, direction="inout")
plt.xlabel('Years')
plt.ylabel(f'daily C root (gC/m2)')
plt.grid(True)
plt.savefig(f"figures_output/daily_time_series/d_croot_{plot_config['pdf_file']}.png", bbox_inches='tight')
'''
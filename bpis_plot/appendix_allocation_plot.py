"""
### Script Description: Analysis of Leaf Carbon and Nitrogen Allocation Under Fire Regimes

This Python script analyzes daily changes in carbon (C) and nitrogen (N) in leaf pools under different fire regimes in a selected biome, using NetCDF data. It also plots the leaf C:N ratio for individual years to visualize nutrient allocation dynamics.

---

#### **Key Functionalities and Steps**

---

### **1. Import Required Libraries**
- **`bpis_plot_support_function`**: Custom library providing support functions, including biome-specific data retrieval.
- **`netCDF4`**: Reads and processes NetCDF files containing geospatial and temporal data.
- **`matplotlib.pyplot`**: Used for plotting results.
- **`numpy`**: Performs numerical operations like reshaping arrays and summing values.

---

### **2. Configuration**
The `run_config` dictionary specifies parameters for the analysis:
- **Biome Selection**: `"cerrado"`.
- **Pixel Identification**: Obtained using a support function (`sf.get_biome_pixel`).
- **Run Collection Path**: Sets the directory for biome-specific data.
- **Scenario Parameters**:
  - **Practice Term**: `"long"` or `"short"` determines the temporal scale of management practices.
  - **Livestock Density**: `"01"` represents 1% livestock presence (choices: `0`, `01`, `05`).
  - **Burning Phase**: Phase within the burning frequency cycle.
  - **Burning Date**: The specific date within the burning cycle.
  - **Start and End Year**: Analysis covers the years 1948–2017.
- Dynamic folder paths are constructed based on the configuration.

---

### **3. Data Loading**
- **Leaf Carbon (`LeafC`) and Nitrogen (`nleaf`)**:
  - Data for daily leaf C and N pools is read from NetCDF files (`d_cleaf.nc` and `d_nleaf.nc`).
  - The data is reshaped into a matrix of years (70 years) by days (365 days), facilitating yearly analysis.

---

### **4. Allocation Calculation**
For each year:
- **Carbon Allocation**: Sum of positive daily increments in leaf carbon (`LeafC`).
- **Nitrogen Allocation**: Sum of positive daily increments in leaf nitrogen (`nleaf`).
- **Allocation Ratio**: Ratio of C allocation to N allocation calculated as \( \text{Allocation Ratio} = \frac{\text{Carbon Allocation}}{\text{Nitrogen Allocation}} \).

These values are stored in lists (`alo_n`, `alo_c`, and `allocation`) for later use.

---

### **5. Visualization**
- The **leaf C:N ratio** is plotted for individual years to observe nutrient dynamics:
  - \( \text{C:N Ratio} = \frac{\text{LeafC}}{\text{nleaf}} \) is computed for each day of the year.
  - Plots are styled with blue solid lines.

---

### **6. Fire Regime Impact**
The script accounts for different fire regimes (`burning_frequency`) to assess how fire frequency influences carbon and nitrogen allocation:
- **No Fire**: Special folder path is configured.
- **Periodic Fires**: Paths are adjusted for specified frequencies, phases, and burning dates.

---

#### **Output**
The script generates yearly plots of the leaf C:N ratio, visualizing how carbon and nitrogen dynamics vary over time. These results provide insights into the impact of fire regimes and management practices on nutrient allocation within the biome.

---

### **Potential Extensions**
- Summarizing allocation data across years for comparative analysis.
- Automating plots for all years or multiple scenarios in a single figure.
- Adding descriptive labels and legends to clarify fire regime contexts in the plots.
"""

import bpis_plot_support_function as sf
from netCDF4 import *
import matplotlib.pyplot as plt
import numpy as np

# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "cerrado"
run_config['pixel'] = sf.get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 2 # 1/2/3/4
run_config['start_year'] = 1948
run_config['end_year'] = 2017

#fig, ax1 = plt.subplots(figsize=(20, 3))
legend_collection = []
linestyle_list = ["solid", "dashed","dashdot","dotted"]
label_list = ["no fire", "10-year","5-year","2-year"]

for i,burning_freq in enumerate([5]):

    run_config['burning_frequency'] = burning_freq
    if run_config['burning_frequency'] == 0 :
        run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
    else :
        run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
        run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
        run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])


    # daily leaf C and N
    # daily leave C
    nc = Dataset("{}/d_cleaf.nc".format(run_config['folder_path']), mode='r')
    leafc = nc.variables['LeafC'][:,0,0] #gC/m2/days
    nc.close()
    leafc = leafc.reshape(70, 365)

    # daily leave N
    nc = Dataset("{}/d_nleaf.nc".format(run_config['folder_path']), mode='r')
    leafn = nc.variables['nleaf'][:,0,0] #gC/m2/days
    nc.close()
    leafn = leafn.reshape(70, 365)

    allocation = []
    alo_n = []
    alo_c = []

    for year in range(0,70):
        leafc_y = leafc[year]
        allocation_c = np.sum(np.maximum(leafc_y[1:] - leafc_y[0:-1],0))
        leafn_y = leafn[year]
        allocation_n = np.sum(np.maximum(leafn_y[1:] - leafn_y[0:-1],0))
        alo_n.append(allocation_n)
        alo_c.append(allocation_c)
        allocation.append(np.divide(allocation_c,allocation_n))

        plt.figure(figsize=(8, 5))
        plt.plot(leafc_y/leafn_y, label="Line 1", color="blue", linestyle="-", marker="")
        plt.show()
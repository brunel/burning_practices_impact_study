# 1. import
import bpis_plot_support_function as sf
import matplotlib.pyplot as plt
import numpy as np
from bpis_plot_support_function import get_biome_pixel


# 2. configuration
# a) run configuration
run_config = dict()
run_config['biome'] = "mata2"
run_config['pixel'] = get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['practice_term'] = "long" # short/long
run_config['livestock_density'] = "01" # 0/01/05
run_config['burning_frequency'] =10 # 0/2/5/10
run_config['burning_phase'] = 0 # [0,burning_frequency,1]
run_config['burning_date'] = 1# 1/2/3/4
run_config['start_year'] = 1948
run_config['scaling'] = False # True/False
if run_config['burning_frequency'] == 0 :
    run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
else :
    run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
    run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
    run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])


# b) figure configuration
plot_config = dict()
plot_config['fig'] = plt.figure(figsize=(25,25))
plot_config['subplots'] = sf.create_subplots(1,8,0.4,0.07,0.005,0.01)
subplots = plot_config['subplots']
plot_config['start_year'] = 1998 # 99 = no el nino
plot_config['end_year'] = plot_config['start_year'] + 2
plot_config['length_period_day'] = (plot_config['end_year']-plot_config['start_year'])*365
plot_config['length_period_month'] = (plot_config['end_year']-plot_config['start_year'])*12
plot_config['length_period_year'] = (plot_config['end_year']-plot_config['start_year'])
if run_config['burning_frequency'] == 0 :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density, no burning"
else :
    plot_config['plot_title'] = f"{run_config['biome']} {run_config['practice_term']} practice, {run_config['livestock_density']} livestock density,burning frequency {run_config['burning_frequency']}, phase {run_config['burning_phase']}, burning date {run_config['burning_date']}"
plot_config['pdf_path'] = "figures_output"
if run_config['burning_frequency'] == 0 :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_nofire_{plot_config['start_year']}"
else :
    plot_config['pdf_file'] = f"{run_config['biome']}_{run_config['practice_term']}_ls{run_config['livestock_density']}_bf{run_config['burning_frequency']}-{run_config['burning_phase']}_bd{run_config['burning_date']}_{plot_config['start_year']}"
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 12
plot_config['legend_font_size'] = 11

# c) import output collection
output_collection = sf.output_dictionnary(run_config)

# 3. Data treatment and plotting

# -) burning months
output_dict = output_collection['annual burning date']
output_dict = sf.import_annual_data(output_dict, run_config, plot_config)
output_collection['monthly burning']['output_array'] = sf.burningdates_to_burningmonths(output_dict['output_array'])

# a) daily precipitation
if run_config['biome'] == "cerrado":
    # extract precipitation data from listing
    output_dict = output_collection['daily precipitation']
    output_dict['daily_output_array'] = sf.extracting_daily_output_from_listing('precipitation',output_dict['file_path'],plot_config)
    var = "precipitation"
    # convert daily to monthly
    output_dict['output_array'] = sf.daily_to_monthly(output_dict['daily_output_array'], 'sum')
elif run_config['biome'] == "mata2":
    output_dict = output_collection['daily temperature']
    output_dict['file_path'] = f"{run_config['run_collection_path']}/{output_dict['file_path']}"
    output_dict['daily_output_array'] = sf.import_daily_data(output_dict,run_config,plot_config)
    # convert daily to monthly
    output_dict['output_array'] = sf.daily_to_monthly(output_dict['daily_output_array'], 'average')
    var = "temperature"
else :
    output_dict = output_collection['monthly precipitation']
    output_dict = sf.import_monthly_data(output_dict, run_config, plot_config)
    var = "precipitation"

if var == "precipitation":
    # plotting - bar plot
    axe = sf.plot_bar_monthly(plot_config,subplots[0],output_dict)
    axe.set_title(plot_config['plot_title'],size=plot_config['title_font_size'],loc="right")
    axe.set_xlim(0,plot_config['length_period_month']-1)
elif var == "temperature":
    axe = sf.plot_monthly(plot_config,subplots[0],output_dict)
    #axe.set_title(plot_config['plot_title'],size=plot_config['title_font_size'],loc="right")
    #axe.set_xlim(0,plot_config['length_period_month']-1)

# b) gwp
# importation co2,ch4 fire emissions + C,N soil emissions
gwp_collection = dict()
gwp_collection['co2_from_fire'] = output_collection['monthly co2 emission from fire']
gwp_collection['ch4_from_fire'] = output_collection['monthly ch4 emission from fire']
gwp_collection['c_from_soil'] = output_collection['daily soil c']
gwp_collection['n_from_soil'] = output_collection['monthly n emission from soil']
gwp_collection['gwp'] = output_collection['annual gwp']
# calculation gwp from gwp-collection
gwp_collection = sf.gwp_calculation(gwp_collection,run_config,plot_config)
gwp_collection['gwp']['output_array'] = gwp_collection['annual_gwp']
# plotting - bar plot
axe = sf.plot_bar_gwp(plot_config,subplots[1],gwp_collection)
#axe = sf.plot_bar_monthly(plot_config,subplots[1],output_dict)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    axe.axis([0,plot_config['length_period_month']-1,0,0.30])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# c) Leave C
output_dict = output_collection['daily leave carbon']
output_dict = sf.import_daily_data (output_dict,run_config,plot_config)
# conversion to gC/m2/month
output_dict['output_array'] = sf.daily_to_monthly(output_dict['output_array'],'mean')
# plotting - plot
axe = sf.plot_monthly(plot_config,subplots[2],output_dict)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    axe.axis([0,plot_config['length_period_month']-1,0,130])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# d) Harvest C
output_dict = output_collection['daily harvest carbon']
output_dict = sf.import_daily_data (output_dict,run_config,plot_config)
# conversion to gC/m2/month
output_dict['output_array'] = sf.daily_to_monthly(output_dict['output_array'],'sum')
#harvest output only count 75% of total harvest
output_dict['output_array'] = output_dict['output_array']/3*4
# plotting - plot
axe = sf.plot_monthly(plot_config,subplots[3],output_dict)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    axe.axis([0,plot_config['length_period_month']-1,0,1.40])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# e) CN ratio
cn_ratio_collection = dict()
cn_ratio_collection['leave_c'] = output_collection['daily leave carbon']
cn_ratio_collection['leave_n'] = output_collection['daily leave nitrogen']
# calculation cn ratio
output_dict = output_collection['leave cn ratio']
output_dict['output_array'] = sf.cnratio_calculation(cn_ratio_collection,run_config,plot_config)
# plotting - plot
axe = sf.plot_monthly(plot_config,subplots[4],output_dict)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    axe.axis([0,plot_config['length_period_month']-1,9,25])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# f) SOM budget
som_collection = sf.som_collection_preprocessing(output_collection,run_config,plot_config)
axe = sf.plot_som_budget(plot_config,subplots[5],som_collection)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    scaling = 1
    axe.axis([0,plot_config['length_period_month']-1,-scaling,scaling])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# g) SMM budget
smm_collection = sf.smm_collection_preprocessing(output_collection,run_config,plot_config)
axe = sf.plot_smm_budget(plot_config, subplots[6], smm_collection)
axe.set_xlim(0,plot_config['length_period_month']-1)
if run_config['scaling'] == True :
    scaling = 1.5
    axe.axis([0,plot_config['length_period_month']-1,-scaling,scaling])
sf.plot_fire_month(axe,output_collection['monthly burning'])

# h) Legend
axe = sf.plot_budget_legend(plot_config, subplots[7], som_collection, smm_collection,output_collection)

plot_config['fig'].savefig(f"{plot_config['pdf_path']}/overview_{plot_config['pdf_file']}.png", bbox_inches='tight',orientation = 'landscape' )
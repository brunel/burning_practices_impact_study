"""
### Script Description

This script visualizes the climatic conditions (temperature and precipitation) of six biomes in South America. The script uses data from NetCDF files, processes the data to compute monthly averages, and generates a combined plot for each biome. The plots display average monthly temperature (line plot) and precipitation (bar chart) side-by-side for a clear comparison. Below is a detailed explanation of the steps:

---

#### 1. **Importing Libraries**
The script imports essential libraries for handling NetCDF files, numerical computations, and plotting:
- `netCDF4`: For reading NetCDF files.
- `pylab` and `matplotlib.pyplot`: For creating visualizations.
- `numpy`: For numerical data manipulation.
- `bpis_plot_support_function`: Custom module providing helper functions (`daily_to_monthly` and `create_subplots`).

---

#### 2. **Defining Biomes and Coordinates**
- **`biome_list`**: Names of six biomes in South America.
- **`coordinate_list`**: Coordinates (longitude, latitude) and unique labels for each biome.

---

#### 3. **Setting Visualization Parameters**
- **Figure Layout**: The `create_subplots` function is used to generate a 3x2 grid layout for plotting.
- **Aesthetic Settings**: Custom font sizes for biome titles, labels, and tick marks.

---

#### 4. **Loading Climate Data**
The script reads three NetCDF files:
1. **`prec.nc`**: Contains monthly precipitation data.
2. **`temp.nc`**: Contains daily temperature data.
3. **`par.nc`**: Contains daily radiation data (though this is read, it is not used in this script).

Each dataset is loaded, relevant variables are extracted, and the files are closed.

---

#### 5. **Processing and Plotting Data**
For each biome:
1. **Extract Location Data**: Match biome coordinates to the latitude and longitude indices in the datasets.
2. **Compute Monthly Averages**:
   - Precipitation: Reshape to monthly values and calculate the mean over all years.
   - Temperature: Convert daily data to monthly averages using `daily_to_monthly` and compute long-term monthly averages.
3. **Create Dual-Axis Plot**:
   - **Left Axis**: Plot average monthly temperature as a red line.
   - **Right Axis**: Plot average monthly precipitation as a blue bar chart.
4. **Add Labels and Titles**:
   - Titles include biome names and labels.
   - Temperature and precipitation are labeled on their respective axes.
5. **Customize Axis Ticks**: Show/hide ticks based on subplot position to maintain clarity in a grid layout.
6. **Set Plot Limits**:
   - Temperature: 13°C to 29°C.
   - Precipitation: 0 mm to 400 mm.

---

#### 6. **Saving the Figure**
The final figure, combining all six biome plots, is saved as a PDF file (`location_climate.pdf`) in landscape orientation.

---

#### Output
The script generates a visually informative PDF summarizing the climate of six biomes, with insights into seasonal temperature and precipitation variations.
"""

# 1. import
from netCDF4 import *
from pylab import *
import numpy as np
from bpis_plot_support_function import daily_to_monthly,create_subplots

biome_list = ["Amazon North",
              "Amazon South",
              "Caatinga",
              "Cerrado",
              "Atlantic Forest",
              "Pampas"]

coordinate_list = [[-64.25, -4.75,"a"],
                   [-58.25, -10.25,"b"],
                   [-40.75, -6.75,"c"],
                   [-48.75, -13.75,"d"],
                   [-41.25, -19.25,"e"],
                   [-54.75, -30.25,"f"]]

months = ['Ja', 'Fe', 'Mr', 'Ap', 'My', 'Jn',
          'Jl', 'Au', 'Se', 'Oc', 'No', 'De']

biometitlesize = 26
labelsize = 25
ticllabelsize = 20

fig = plt.figure(figsize=(30,30))
subplots = create_subplots(3,2,0.2,0.2,0.01,0.01)

# get monthly precipitation
nc = Dataset("C:/Users/brune\Documents\local_data_cluster\climate_output/prec.nc", mode='r')
latitude_list = nc.variables['latitude'][:]
longitude_list = nc.variables['longitude'][:]
monthly_precipitation_array = nc.variables['prec'][:,:,:]
nc.close()

# get daily temperature
nc = Dataset("C:/Users/brune\Documents\local_data_cluster\climate_output/temp.nc", mode='r')
daily_temperature_array = nc.variables['temp'][:,:,:]
nc.close()

# get daily radiation
nc = Dataset("C:/Users/brune\Documents\local_data_cluster\climate_output/par.nc", mode='r')
daily_par_array = nc.variables['PAR'][:,:,:]
nc.close()

for id_biome,biome in enumerate(biome_list) :

    coordinate_biome = coordinate_list[id_biome]
    latitude = np.where(latitude_list == coordinate_biome[1])[0][0]
    longitude = np.where(longitude_list == coordinate_biome[0])[0][0]

    # precipitation
    monthly_precipitation = monthly_precipitation_array[:,latitude,longitude].reshape(71, 12)
    monthly_average_precipitation = monthly_precipitation.mean(axis=0)

    # temperature
    monthly_temperature = daily_to_monthly(daily_temperature_array[:,latitude,longitude], "mean").reshape(71, 12)
    monthly_average_temperature = monthly_temperature.mean(axis=0)

    # Create the figure and first axis for temperature
    ax1 = fig.add_axes(subplots[id_biome])

    # Plot temperature
    ax1.plot(months, monthly_average_temperature, color='red', marker='o', label='Temperature (°C)')

    # Create the second axis for precipitation
    ax2 = ax1.twinx()
    ax2.bar(months, monthly_average_precipitation, color='blue', alpha=0.6, label='Precipitation (mm)')

    if id_biome <= 2 :
        ax1.set_xticklabels([""])
    if id_biome in [0,3]:
        ax1.set_ylabel('Temperature (°C)', fontsize=labelsize)
        ax2.set_yticklabels([""])
    elif id_biome in [2,5]:
        ax2.set_ylabel('Precipitation (mm)',fontsize=labelsize)
        ax1.set_yticklabels([""])
    else :
        ax1.set_yticklabels([""])
        ax2.set_yticklabels([""])

    ax1.set_ylim(13, 29)
    ax2.set_ylim(0, 400)
    ax1.tick_params(labelsize=ticllabelsize)
    ax2.tick_params(labelsize=ticllabelsize)

    # Title and layout
    ax1.text(0,28.5,f"{biome} ({coordinate_biome[2]})",
             fontsize = biometitlesize,weight="bold",rotation="horizontal",
             va="top",ha="left")

fig.savefig("figures_output/location_climate.pdf", bbox_inches='tight',orientation = 'landscape')
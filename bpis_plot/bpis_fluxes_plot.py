"""
### Script Description

This script generates visualizations of nitrogen and soil nitrogen flux budgets for different practice scenarios and burning frequencies in various biomes. It processes data, computes flux averages, and creates detailed plots to analyze the impact of burning practices over time.

### Key Features:

1. **Configuration**:
   - The script dynamically adjusts settings based on the selected biome, practice term, and burning frequency.
   - It distinguishes between nitrogen and soil nitrogen budgets, setting appropriate axis limits and file paths.

2. **Data Processing**:
   - Data for positive and negative nitrogen fluxes is imported and averaged over multiple phases for each configuration.
   - Flux data is categorized into distinct components (e.g., deposition, leaching, uptake) with predefined color schemes.

3. **Plot Generation**:
   - Stacked bar charts visualize flux contributions for each practice scenario and burning frequency.
   - Both primary and secondary x-axes are used to display practice scenarios and burning frequencies clearly.
   - Positive and negative fluxes are plotted separately, with a horizontal line at zero to distinguish net contributions.

4. **Legend Creation**:
   - Separate legends are generated for positive and negative fluxes, detailing the flux components with corresponding colors.
   - Legends are saved as standalone images for easier use in reports or publications.

5. **Output**:
   - The script saves the generated plots and legends as PNG files, organized by biome, budget type, and livestock density.

This script provides a detailed, customizable framework for analyzing nitrogen flux dynamics across various land management scenarios, aiding in understanding the ecological impacts of burning practices.
"""

# 1. import
import bpis_plot_support_function as sf
import matplotlib.pyplot as plt
import numpy as np
from bpis_plot_support_function import get_biome_pixel
import matplotlib.patches as mpatches

positive_fluxes_names = ["Litter Fall","Manure","Deposition","Ashes","BNF"]
positive_fluxes_color = ['#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6']
negative_fluxes_names = ["Leaching","Nitrification-\nDenitrification","Volatilisation","Uptake"]
negative_fluxes_color = ['#a6cee3','#1f78b4','#b2df8a','#6a3d9a']

nitro_negative_fluxes_names = negative_fluxes_names[0:3]
nitro_negative_fluxes_names.append("Harvest")
nitro_negative_fluxes_color = negative_fluxes_color[0:3]
nitro_negative_fluxes_color.append("#33a02c")
nitro_negative_fluxes_names.append("Fire emission")
nitro_negative_fluxes_color.append("#b15928")


# a) run configuration
run_config = dict()
plot_config = dict()
run_config['biome'] = ("mata1") # mata1 mata2 caatinga cerrado
plot_config['budget'] = "nitrogen" # soiln nitrogen carbon
ylim_nitrogen=(-3, 3)
ylim_soiln=(-11, 11)
run_config['pixel'] = get_biome_pixel (run_config['biome'])
run_config['run_collection_path'] = "C:/Users/brune\Documents\local_data_cluster/{}_{}/".format(run_config['biome'],run_config['pixel'])
run_config['livestock_density'] = "01" # 0/01/05
run_config['start_year'] = 1948

if run_config['biome'] in ["amazon1","amazon2","caatinga"]:
    run_config['burning_date'] = 1
elif run_config['biome'] in ["cerrado","mata1"]:
    run_config['burning_date'] = 2
elif run_config['biome'] in ["pampas"]:
    run_config['burning_date'] = 3
else :
    exit("no burning date for this biome")

# 2. configuration
# b) figure configuration
plot_config['title_font_size'] = 14
plot_config['axis_label_font_size'] = 15
plot_config['legend_font_size'] = 11

practice_scenarios = ['First 20\nyears', 'Recent\ndist.', 'Pre-established\ndist.']  # Blocks on the x-axis
frequency_scenarios = ['No fire', '10-year', '5-year', '2-year']  # Bars within each block
frequency_list = [0,10,5,2]
phase_range = [1,10,5,2]

positive_fluxes = []
negative_fluxes = []

# fill up positives fluxes and negatives fluxes
for practice in practice_scenarios :
    sub_positive_fluxes = []
    sub_negative_fluxes = []
    for id_frequency,frequency in enumerate(frequency_list) :

        if practice == 'Pre-established\ndist.':
            run_config['practice_term'] = "long"
            plot_config['start_year'] = 1997
            plot_config['end_year'] = 2017
        else :
            run_config['practice_term'] = "short"
            if practice == 'First 20\nyears':
                plot_config['start_year'] = 1948
                plot_config['end_year'] = 1968
            else :
                plot_config['start_year'] = 1997
                plot_config['end_year'] = 2017


        run_config['burning_frequency'] = frequency


        phase_sub_positive_fluxes = []
        phase_sub_negative_fluxes = []
        # loop over phase
        for phase in range (phase_range[id_frequency]):
            run_config['burning_phase'] = phase
            if run_config['burning_frequency'] == 0 :
                run_config['folder_path'] = "{}{}_{}term_ld{}_nofire".format(
                run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'])
            else :
                run_config['folder_path'] = "{}{}_{}term_ld{}_fq{}-{}_bd{}".format(
                run_config['run_collection_path'],run_config['pixel'],run_config['practice_term'],run_config['livestock_density'],
                run_config['burning_frequency'],run_config['burning_phase'],run_config['burning_date'])

            if plot_config['budget'] == "soiln":
                phase_sub_positive_fluxes.append(sf.import_soiln_positive_fluxes(run_config,plot_config))
                phase_sub_negative_fluxes.append(sf.import_soiln_negative_fluxes(run_config,plot_config))
            elif plot_config['budget'] == "nitrogen":
                phase_sub_positive_fluxes.append(sf.import_nitrogen_positive_fluxes(run_config,plot_config))
                fluxes = sf.import_nitrogen_negative_fluxes(run_config,plot_config)
                phase_sub_negative_fluxes.append(fluxes)

        sub_positive_fluxes.append(np.average(phase_sub_positive_fluxes,axis=0))
        sub_negative_fluxes.append(np.average(phase_sub_negative_fluxes,axis=0))

    positive_fluxes.append(sub_positive_fluxes)
    negative_fluxes.append(sub_negative_fluxes)


# Convert lists to numpy arrays for easier stacking
positive_fluxes = np.array(positive_fluxes)
negative_fluxes = np.array(negative_fluxes)

# Define dimensions
num_practices = len(practice_scenarios)
num_frequencies = len(frequency_scenarios)
num_positive_fluxes = positive_fluxes.shape[2]
num_negative_fluxes = negative_fluxes.shape[2]

# Set up bar width and positions
bar_width = 0.08
sub_bar_width = bar_width + 0.01
x = np.asarray([0,0.5,1])

# Create figure and axis
fig, ax = plt.subplots(figsize=(5, 8))

if plot_config['budget'] == "soiln":
    # Plotting positive and negative fluxes for each frequency within each practice scenario
    for i in range(num_frequencies):
        for j in range(num_positive_fluxes):
            if i == 0:
                ax.bar(x + i * bar_width, positive_fluxes[:, i, j], bar_width,color = positive_fluxes_color[j],
                   bottom=np.sum(positive_fluxes[:, i, :j],  axis=1),label=f'{positive_fluxes_names[j]}')
            else :
                ax.bar(x + i * sub_bar_width, positive_fluxes[:, i, j], bar_width,color = positive_fluxes_color[j],
                       bottom=np.sum(positive_fluxes[:, i, :j],  axis=1),)

        for j in range(num_negative_fluxes):
            if i == 0:
                ax.bar(x + i * bar_width, negative_fluxes[:, i, j], bar_width,color = negative_fluxes_color[j],
                       bottom=np.sum(negative_fluxes[:, i, :j],  axis=1),label=f'{negative_fluxes_names[j]}')
            else :
                ax.bar(x + i * sub_bar_width, negative_fluxes[:, i, j], bar_width,color = negative_fluxes_color[j],
                       bottom=np.sum(negative_fluxes[:, i, :j],  axis=1),)

    # Add labels and title
    ax.set_ylabel('Nitrogen Fluxes (gN/m2)',fontsize=17)
    plt.yticks(fontsize = 14)
    ax.set_title(f'Nitrogen Budget {run_config["biome"]} {run_config["livestock_density"]}',fontsize=15)

    # Plot the horizontal line at y=0
    ax.axhline(0, color="#bdbdbd", linewidth=1.5, linestyle='--')
    ax.set(ylim=ylim_soiln)


if plot_config['budget'] == "nitrogen":

    # Plotting positive and negative fluxes for each frequency within each practice scenario
    for i in range(num_frequencies):

        for j in range(num_positive_fluxes):

            if i == 0:
                ax.bar(x + i * bar_width, 0.0002, bar_width,color = "#ffffff",
                   bottom=positive_fluxes[:,i,j],label=f'{positive_fluxes_names[j]}')
            else :
                ax.bar(x + i * sub_bar_width, 0.0002, bar_width,color = "#ffffff",
                       bottom=positive_fluxes[:,i,j])

        for j in range(num_negative_fluxes):
            if i == 0:
                if j == 0:
                    ax.bar(x + i * bar_width, negative_fluxes[:, i, j], bar_width,color = nitro_negative_fluxes_color[j],
                           bottom=positive_fluxes[:,i,0],label=f'{nitro_negative_fluxes_names[j]}')
                else :
                    ax.bar(x + i * bar_width, negative_fluxes[:, i, j], bar_width,color = nitro_negative_fluxes_color[j],
                           bottom=positive_fluxes[:,i,0]+np.sum(negative_fluxes[:, i, :j],axis=1),label=f'{nitro_negative_fluxes_names[j]}')
            else :
                if j == 0:
                    ax.bar(x + i * sub_bar_width, negative_fluxes[:, i, j], bar_width,color = nitro_negative_fluxes_color[j],
                           bottom=positive_fluxes[:,i,0])
                else :
                    ax.bar(x + i * sub_bar_width, negative_fluxes[:, i, j], bar_width,color = nitro_negative_fluxes_color[j],
                           bottom=positive_fluxes[:,i,0]+np.sum(negative_fluxes[:, i, :j],axis=1))


    # Add labels and title
    ax.set_ylabel('Nitrogen Fluxes (gN/m2)',fontsize=17)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 14)
    ax.set_title(f'Nitrogen Budget {run_config["biome"]}',fontsize=15)

    ax.axhline(0, color="#bdbdbd", linewidth=1.5, linestyle='--')
    ax.set(ylim=ylim_nitrogen)


# Set x-ticks and labels
ax.set_xticks(x + (num_frequencies - 1) * bar_width / 2)
ax.set_xticklabels(practice_scenarios,fontsize=12)

# Enable minor ticks
ax.minorticks_on()

# Set minor tick positions and labels (frequency scenarios)
minor_tick_positions = x[:, np.newaxis] + np.arange(num_frequencies) * (bar_width+0.01)
minor_tick_positions = minor_tick_positions.flatten()
ax.set_xticks(minor_tick_positions, minor=True)
ax.set_xticklabels(frequency_scenarios * num_practices, minor=True, rotation=90, va='bottom',ha='center', fontsize=16)
ax.tick_params(axis="x",which='minor',direction="in", pad=-10)


# Create secondary x-axis above the plot for practice scenarios
secax1 = ax.secondary_xaxis('top')
secax1.set_xticks(x + (num_frequencies - 1) * bar_width / 2)
secax1.set_xticklabels(practice_scenarios, fontsize=16)

ax.set_xticks([])  # Remove major ticks to avoid duplication

# Adjust major tick parameters to make minor ticks more visible
ax.tick_params(axis='x', which='major', pad=20, size=0)  # Increase padding between major tick labels and the axis
ax.tick_params(axis='x', which='minor', length=4, color='gray')  # Customize minor ticks

# Show the plot
plt.tight_layout()

fig.savefig(f"figures_output/{run_config['biome']}_{plot_config['budget']}_budget_{run_config['livestock_density']}.png", bbox_inches='tight', orientation='landscape')

# LEGEND
marker_small = 5
marker_big = 10

# positive fluxes
legend = []
for id_fluxes,fluxes in enumerate(positive_fluxes_names) :
    legend.append(mpatches.Patch(facecolor=positive_fluxes_color[id_fluxes], label=fluxes))

fig = plt.figure(figsize=(25, 25))
subplots = sf.set_subplots(1, 1, 0.2, 0.2, 0.01, 0.06)
axe = fig.add_axes(subplots[0],frame_on=False, yticks=[],xticks=[])
axe.legend(handles = legend,fontsize=18,loc = 'lower left',ncol=3)
axe.grid()

fig.savefig(f"figures_output/legend_positive_fluxes.png", bbox_inches='tight', orientation='landscape')

# negative fluxes
legend = []
for id_fluxes,fluxes in enumerate(negative_fluxes_names) :
    legend.append(mpatches.Patch(facecolor=negative_fluxes_color[id_fluxes], label=fluxes))

# add harvest
legend.append(mpatches.Patch(facecolor="#33a02c", label="Harvest N"))
legend.append(mpatches.Patch(facecolor="#b15928", label="NOx Emission"))

fig = plt.figure(figsize=(25, 25))
subplots = sf.set_subplots(1, 1, 0.2, 0.2, 0.01, 0.06)
axe = fig.add_axes(subplots[0],frame_on=False, yticks=[],xticks=[])
axe.legend(handles = legend,fontsize=18,loc = 'lower left',ncol=3)
axe.grid()

fig.savefig(f"figures_output/legend_negative_fluxes.png", bbox_inches='tight', orientation='landscape')


"""### Script Description: Mapping Brazilian Biomes and Study Sites with Geopandas, Matplotlib, and Cartopy

This Python script visualizes Brazilian biomes and study sites on a map, leveraging the `geopandas`, `matplotlib`, and `cartopy` libraries. The steps and functionalities of the script are as follows:

---

#### **1. Library Imports**
- **`geopandas`**: Handles geospatial data (shapefiles) and performs spatial operations.
- **`matplotlib.pyplot`**: Used for creating and customizing the map plot.
- **`cartopy`**: Provides map projections and geospatial data visualizations.
- **`matplotlib.lines`**: Creates custom legend entries.

---

#### **2. Configuration**
- Font sizes for tick labels, site markers, and legend are predefined to ensure consistent styling.
- Colors are assigned to each Brazilian biome using a dictionary, `biome_colors`, to facilitate visualization.

---

#### **3. Load Geospatial Data**
- **Brazil’s border shapefile** is loaded to outline the country’s boundaries.
- **Brazilian biomes shapefile** is loaded to display the different biome regions.
- Both datasets are reprojected to the `EPSG:4326` coordinate reference system (latitude-longitude) if needed, ensuring compatibility with the map projection.

---

#### **4. Map Plotting**
- A map is created using the **PlateCarree projection** for geographic accuracy.
- Brazil's border is outlined in black.
- Biomes are filled with distinct colors from the `biome_colors` dictionary.
- Coastlines and borders are added as additional geographic features.
- Gridlines with latitude and longitude labels are overlaid for reference.

---

#### **5. Study Sites**
- Predefined study sites, represented by their coordinates and labels, are plotted as black dots.
- Text labels (letters) are added near each site to identify them on the map.

---

#### **6. Custom Legend**
- A legend is created to explain the map markers, including study sites and a placeholder for additional features.

---

#### **7. Display**
- The map is rendered with:
  - Customized axis tick labels.
  - Black dots marking study site locations.
  - A legend positioned in the bottom right corner.

---

### **Visual Features**
- **Biomes**: Color-coded regions representing major ecological zones in Brazil.
- **Study Sites**: Black dots with labeled text indicating specific research locations.
- **Geographic Features**: Coastlines and borders enhance map readability.

---

### **Output**
The resulting map highlights Brazilian biomes with distinct colors, marks study sites, and provides a legend for easy interpretation. The visualization is tailored for ecological or geographic studies, emphasizing both regional diversity and specific points of interest.
"""

import geopandas as gpd
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
import cartopy.crs as ccrs
import matplotlib.lines as mtplines

ticklabelsize = 15
lettersitesize = 17
legendsize = 16

biome_colors = {
    "Caatinga": "#bebada",      # Bleu
    "Cerrado": "#ffffb3",       # Orange
    "Pantanal": "#fb8072",      # Vert
    "Pampa": "#80b1d3",         # Rouge
    "Amazônia": "#8dd3c7",      # Violet
    "Mata Atlântica": "#fdb462" # Marron
}

# Load Brazil's border shapefile
brazil_border = gpd.read_file("brazil_Brazil_Country_Boundary_MAPOG_shapefile/brazil_Brazil_Country_Boundary.shp")

# Load Biomes shapefile
biomes = gpd.read_file("brazilian_biome_shapefile\Brazil_biomes.shp")

# Check and reproject CRS if necessary
if biomes.crs != "EPSG:4326":
    biomes = biomes.to_crs("EPSG:4326")

# Reproject border if necessary
if brazil_border.crs != "EPSG:4326":
    brazil_border = brazil_border.to_crs("EPSG:4326")

# Create the plot
fig, ax = plt.subplots(1, 1, figsize=(12, 10), subplot_kw={"projection": ccrs.PlateCarree()})

# Add Brazil's border
brazil_border.plot(ax=ax, edgecolor="black", facecolor="none", linewidth=1, label="Brazil Border")

# Plot biomes with different colors
biomes["color"] = biomes["name"].map(biome_colors)
biomes.plot(ax=ax, column="name", legend=False, color=biomes["color"])

# Add coastlines and gridlines
ax.add_feature(cfeature.COASTLINE, edgecolor="black", linewidth=0.5)
ax.add_feature(cfeature.BORDERS, linestyle=":", edgecolor="gray", linewidth=0.5)
ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False)

study_sites = [[-40.75, -6.75,"c"], #caatinga
               [-48.75, -13.75,"d"], #cerrado
               [-54.75, -30.25,"f"], #pampa
               [-64.25, -4.75,"a"], #amazon NW
               [-58.25, -10.25,"b"], #amazon SE
               [-41.25, -19.25,"e"]] #atlantic forest north

for site in study_sites :
    ax.plot(site[0],site[1],"o",markersize=6,markeredgecolor="#000000", markerfacecolor="#000000")
    ax.text(site[0]+0.1,site[1]+0.1,site[2],
            fontsize = lettersitesize,
            weight="bold",
            rotation="horizontal",
            #transform=fig.transFigure,
            va="bottom",
            ha="left")

plt.xticks(fontsize=ticklabelsize)
plt.yticks(fontsize=ticklabelsize)

#legend
legends = [
    mtplines.Line2D([0], [0], color="#000000", linewidth=0, linestyle=None,marker="o",markersize=6, label='Study Sites'),
    mtplines.Line2D([0], [0], color="#000000", linewidth=0, linestyle=None, label='Seasonality Transition Line')
    ]

ax.legend(handles = legends,fontsize=legendsize,loc = 'lower right',ncol=1)


# Show the map
plt.show()
